//
//  ImagePreViewCell.h
//  Design Share
//
//  Created by Gautham on 22/10/13.
//  Copyright (c) 2013 Pramati. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImagePreViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@end
