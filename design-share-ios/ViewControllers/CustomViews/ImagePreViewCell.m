//
//  ImagePreViewCell.m
//  Design Share
//
//  Created by Gautham on 22/10/13.
//  Copyright (c) 2013 Pramati. All rights reserved.
//

#import "ImagePreViewCell.h"

@implementation ImagePreViewCell
@synthesize imageView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
