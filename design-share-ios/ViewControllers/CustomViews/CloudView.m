//
//  CloudView.m
//  design-share-ios
//
//  Created by Gautham on 15/11/13.
//  Copyright (c) 2013 Pramati. All rights reserved.
//

#import "CloudView.h"

@implementation CloudView
@synthesize destinationImage,sourceImage,processLabel;
@synthesize img1,img2,img3;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

+ (id)cloudView
{
    CloudView *customView = [[[NSBundle mainBundle] loadNibNamed:@"CloudView" owner:nil options:nil] lastObject];
    
    // make sure customView is not nil or the wrong class!
    if ([customView isKindOfClass:[CloudView class]])
    {
        UIImage *image1 = [UIImage imageNamed:@"ico_pagination_dot_sel"];
        UIImage *image2 = [UIImage imageNamed:@"ico_pagination_dot_unsel"];
        customView.img1.animationImages = [NSArray arrayWithObjects:image1,image2,image2, nil];
        customView.img2.animationImages = [NSArray arrayWithObjects:image2,image1,image2, nil];
        customView.img3.animationImages = [NSArray arrayWithObjects:image2,image2,image1, nil];
       
        [customView.img1 setAnimationDuration:1];
        [customView.img2 setAnimationDuration:1];
        [customView.img3 setAnimationDuration:1];
        
        [customView.img1 startAnimating];
        [customView.img2 startAnimating];
        [customView.img3 startAnimating];

        return customView;
    }
    else
        return nil;
}

-(void)setupViewforUpload:(NSString *)state
{
    if ([state isEqualToString:@"upload"])
    {
        [sourceImage setImage:[UIImage imageNamed:@"ico_iPhone"]];
        [destinationImage setImage:[UIImage imageNamed:@"ico_cloud"]];
        [processLabel setText:@"Uploading Design"];
    }
    else if ([state isEqualToString:@"download"])

    {
        [sourceImage setImage:[UIImage imageNamed:@"ico_cloud"]];
        [destinationImage setImage:[UIImage imageNamed:@"ico_iPhone"]];
        [processLabel setText:@"Downloading Data"];
    }
    else if ([state isEqualToString:@"image"])
    {
        [sourceImage setImage:[UIImage imageNamed:@"ico_web"]];
        [destinationImage setImage:[UIImage imageNamed:@"ico_iPhone"]];
        [processLabel setText:@"Downloading Data"];
    }

}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
