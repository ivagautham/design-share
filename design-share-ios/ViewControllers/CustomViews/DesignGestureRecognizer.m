//
//  DesignGestureRecognizer.m
//  DesignShare
//
//  Created by Bastin Raj on 11/8/13.
//  Copyright (c) 2013 Pramati. All rights reserved.
//

#import "DesignGestureRecognizer.h"

@implementation DesignGestureRecognizer

@synthesize callBackDelegate;

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView: touch.view];
    [self.callBackDelegate componentTouchesBegan: point];
}

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesMoved:touches withEvent:event];
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView: touch.view];
    [self.callBackDelegate componentTouchesMoved: point];
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView: touch.view];
    [self.callBackDelegate componentTouchesEnded: point];
}

- (void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesCancelled:touches withEvent:event];
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView: touch.view];
    [self.callBackDelegate componentTouchesCancelled: point];
}

@end
