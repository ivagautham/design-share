//
//  DesignGestureRecognizer.h
//  DesignShare
//
//  Created by Bastin Raj on 11/8/13.
//  Copyright (c) 2013 Pramati. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIKit/UIGestureRecognizerSubclass.h>
#import "DesignShareGestureDelegate.h"

@interface DesignGestureRecognizer : UIGestureRecognizer<UIGestureRecognizerDelegate>


@property(nonatomic, retain) id<DesignShareGestureDelegate> callBackDelegate;

@end
