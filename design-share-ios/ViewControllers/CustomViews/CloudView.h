//
//  CloudView.h
//  design-share-ios
//
//  Created by Gautham on 15/11/13.
//  Copyright (c) 2013 Pramati. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CloudView : UIView
{
    IBOutlet UIImageView *sourceImage;
    IBOutlet UIImageView *destinationImage;
    IBOutlet UILabel *processLabel;
    IBOutlet UIImageView *img1;
    IBOutlet UIImageView *img2;
    IBOutlet UIImageView *img3;

}

@property(nonatomic) UIImageView *sourceImage;
@property(nonatomic) UIImageView *destinationImage;
@property(nonatomic) UILabel *processLabel;

@property(nonatomic) UIImageView *img1;
@property(nonatomic) UIImageView *img2;
@property(nonatomic) UIImageView *img3;

+ (id)cloudView;
-(void)setupViewforUpload:(NSString *)state;

@end
