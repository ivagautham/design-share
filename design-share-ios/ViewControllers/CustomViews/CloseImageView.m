//
//  CloseImageView.m
//  design-share-ios
//
//  Created by Bastin Raj on 12/9/13.
//  Copyright (c) 2013 Pramati. All rights reserved.
//

#import "CloseImageView.h"

@implementation CloseImageView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
