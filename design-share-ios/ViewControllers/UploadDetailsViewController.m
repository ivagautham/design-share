//
//  UploadDetailsViewController.m
//  design-share-ios
//
//  Created by Gautham on 11/11/13.
//  Copyright (c) 2013 Pramati. All rights reserved.
//

#import "UploadDetailsViewController.h"
#import "WebServiceManager.h"
#import "CloudView.h"
#import "PreviewLinkViewController.h"

#define kOFFSET_FOR_KEYBOARD 80.0

@interface UploadDetailsViewController ()
{
    UploadStatusViewController *mUploadStatusViewController;
    CloudView *cloudView;
    DocumentsDirectoryViewController *mDocumentsDirectoryViewController;
    PreviewLinkViewController *mPreviewLinkViewController;
}

@end

@implementation UploadDetailsViewController
@synthesize imageView;
@synthesize imageAuthor,imageAuthorMail,imageFor,imageName,imageResolution,imageSize,imageType,author,email;
@synthesize postDesign,trash,link;
@synthesize imageToUpdate, imageData, orientation;
@synthesize source,sourcePath;
@synthesize allDesignsCollection,allDesignsImageArray,allDesignsDetailArray;
@synthesize uploadOptionSheet;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    imageView.layer.borderWidth = 1;
    imageView.layer.borderColor = [[UIColor blackColor] CGColor];
    imageName.delegate = self;
    author.delegate = self;
    email.delegate = self;
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle: nil];
    mUploadStatusViewController = [storyboard instantiateViewControllerWithIdentifier:@"UploadStatusViewController"];
    mUploadStatusViewController.navigationItem.hidesBackButton = YES;
    mUploadStatusViewController.delegate = self;
    
    mDocumentsDirectoryViewController =[storyboard instantiateViewControllerWithIdentifier:@"DocumentsDirectoryViewController"];
    mDocumentsDirectoryViewController.navigationItem.hidesBackButton = YES;
    
    mPreviewLinkViewController = [storyboard instantiateViewControllerWithIdentifier:@"PreviewLinkViewController"];
    mPreviewLinkViewController.navigationItem.hidesBackButton = YES;
    
    cloudView = [CloudView cloudView];
    [cloudView setupViewforUpload:@"do"];
    
    uploadOptionSheet = [[UIActionSheet alloc] initWithTitle:@"Upload from " delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera Roll", @"Documents Directory",@"URL", nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localNotificationhandler:) name:NOTIFICATION_UPLOAD_DESIGN_SUCCESS object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localNotificationhandler:) name:NOTIFICATION_UPLOAD_DESIGN_FAILED object:nil];
}

-(void)keyboardWillShow
{
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)keyboardWillHide
{
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    //move the main view, so that the keyboard does not hide it.
    //    if  (self.view.frame.origin.y >= 0)
    //    {
    //        [self setViewMovedUp:YES];
    //    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden: NO];
    [self.view setUserInteractionEnabled:TRUE];
    
    if(!allDesignsImageArray)
        allDesignsImageArray = [[NSMutableArray alloc] init];
    if (allDesignsImageArray.count == 0)
        [allDesignsImageArray addObject:imageToUpdate];
    
    if(!allDesignsDetailArray)
        allDesignsDetailArray = [[NSMutableArray alloc] init];
    if (allDesignsDetailArray.count == 0)
        [allDesignsDetailArray addObject:imageData];
    
    [allDesignsCollection reloadData];
    [self loadViewWithImage:imageToUpdate andData:imageData];
    
    // register for keyboard notifications
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(keyboardWillShow)
    //                                                 name:UIKeyboardWillShowNotification
    //                                               object:nil];
    //
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(keyboardWillHide)
    //                                                 name:UIKeyboardWillHideNotification
    //                                               object:nil];
    
}

- (void) localNotificationhandler:(NSNotification *) notification
{
    [self.view setUserInteractionEnabled:TRUE];
    if ([[notification name] isEqualToString:NOTIFICATION_UPLOAD_DESIGN_FAILED])
    {
        [cloudView removeFromSuperview];
        if (!(mUploadStatusViewController.status))
            mUploadStatusViewController.status = [[NSString alloc] init];
        mUploadStatusViewController.status = NOTIFICATION_UPLOAD_DESIGN_FAILED;
        [self presentViewController:mUploadStatusViewController animated:TRUE completion:^{
            [allDesignsImageArray removeAllObjects];
            [allDesignsDetailArray removeAllObjects];
            [mUploadStatusViewController updateStatus:NOTIFICATION_UPLOAD_DESIGN_FAILED withResponse:nil];
        }];
        
    }
    else if ([[notification name] isEqualToString:NOTIFICATION_UPLOAD_DESIGN_SUCCESS])
    {
        NSString *responseObj = [notification object];
        NSData* data = [responseObj dataUsingEncoding:NSUTF8StringEncoding];
        
        NSError *error;
        NSDictionary * responseDict = [NSJSONSerialization JSONObjectWithData:data options: NSJSONReadingMutableContainers error:&error];
        
        [cloudView removeFromSuperview];
        if (!(mUploadStatusViewController.status))
            mUploadStatusViewController.status = [[NSString alloc] init];
        mUploadStatusViewController.status = NOTIFICATION_UPLOAD_DESIGN_SUCCESS;
        
        if (!(mUploadStatusViewController.responseObject))
            mUploadStatusViewController.responseObject = [[NSMutableDictionary alloc] init];
        [mUploadStatusViewController.responseObject removeAllObjects];
        [mUploadStatusViewController.responseObject addEntriesFromDictionary:responseDict];
        
        [self presentViewController:mUploadStatusViewController animated:TRUE completion:^{
            [allDesignsImageArray removeAllObjects];
            [allDesignsDetailArray removeAllObjects];
            [mUploadStatusViewController updateStatus:NOTIFICATION_UPLOAD_DESIGN_SUCCESS withResponse:responseDict];
        }];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    //    [[NSNotificationCenter defaultCenter] removeObserver:self
    //                                                    name:UIKeyboardWillShowNotification
    //                                                  object:nil];
    //
    //    [[NSNotificationCenter defaultCenter] removeObserver:self
    //                                                    name:UIKeyboardWillHideNotification
    //                                                  object:nil];
    if ([author isFirstResponder])
        [author resignFirstResponder];
    if ([email isFirstResponder])
        [email resignFirstResponder];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSDictionary *)makeDictionaryforCurrentDesign
{
    NSString *Imgsource = [imageData valueForKey:@"source"];
    
    if ([Imgsource isEqualToString:@"camera_roll"])
    {
        NSURL *imagePath = [imageData objectForKey:@"UIImagePickerControllerReferenceURL"];
        ALAssetsLibraryAssetForURLResultBlock resblock = ^(ALAsset *imageAsset)
        {
            ALAssetRepresentation *imageRep = [imageAsset defaultRepresentation];
            
            NSString *nameString = [imageRep filename];
            NSString *sizeString = [NSString stringWithFormat:@"%lld bytes", [imageRep size]];
            NSString *typeString = [self getImageKindforImage:[imageRep filename]];
            CGFloat height = imageToUpdate.size.height;
            CGFloat width = imageToUpdate.size.width;
            
            NSString *dimensionString = [NSString stringWithFormat:@"%.0fx%.0f",width,height];
            NSString *desinged_ForString = [self getDesignedForValueForImageSize:imageToUpdate.size];
            NSString *authorString = author.text;
            NSString *emailString = email.text;
            
            NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:authorString,@"author_name",emailString,@"author_email",@"reviewer@gmail.com",@"reviewer_email", nameString,@"name",typeString,@"type",dimensionString,@"dimension",desinged_ForString, @"designed_for",sizeString,@"size", nil];
            
            //return dict;
            
        };
        
        // get the asset library and fetch the asset based on the ref url (pass in block above)
        ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
        [assetslibrary assetForURL:imagePath resultBlock:resblock failureBlock:nil];
    }
    else if([Imgsource isEqualToString:@"documents_directory"] || [Imgsource isEqualToString:@"url"])
    {
        NSString *nameString = [imageData valueForKey:@"file_path"];
        NSString *typeString = [self getImageKindforImage:nameString];
        NSString *sizeString = [NSString stringWithFormat:@"%@ bytes",[imageData valueForKey:NSFileSize]];
        
        CGFloat height = imageToUpdate.size.height;
        CGFloat width = imageToUpdate.size.width;
        NSString *dimensionString = [NSString stringWithFormat:@"%.0fx%.0f",width,height];
        NSString *desinged_ForString = [self getDesignedForValueForImageSize:imageToUpdate.size];
        NSString *authorString = author.text;
        NSString *emailString = email.text;
        
        NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:authorString,@"author_name",emailString,@"author_email",@"reviewer@gmail.com",@"reviewer_email", nameString,@"name",typeString,@"type",dimensionString,@"dimension",desinged_ForString, @"designed_for",sizeString,@"size", nil];
        
        return dict;
    }
    
    return nil;
}

-(IBAction)onPostDesign:(id)sender
{
    NSString *invalidString = @"Please enter the author name";
    
    if ([author.text length] <= 0)
    {
        UIAlertView *invalidAuthor = [[UIAlertView alloc] initWithTitle:@"Invalid Author" message:invalidString delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [invalidAuthor show];
        return;
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setObject:author.text forKey:@"Default_Author"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    BOOL validEmail = [self NSStringIsValidEmail:email.text];
    if (!validEmail)
    {
        invalidString = @"Email appears to be invalid";
        if ([email.text length] > 0)
            invalidString = [NSString stringWithFormat:@"%@ : %@",invalidString, email.text];
        
        UIAlertView *invalidEmail = [[UIAlertView alloc] initWithTitle:@"Invalid E-Mail" message:invalidString delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [invalidEmail show];
        return;
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setObject:email.text forKey:@"Default_Email"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    [cloudView setFrame:CGRectMake(10, 124, 300, 320)];
    cloudView.layer.cornerRadius = 15;
    cloudView.layer.borderWidth = 0.5;
    cloudView.layer.borderColor = [UIColor whiteColor].CGColor;
    
    [self.view addSubview:cloudView];
    [self.view setUserInteractionEnabled:FALSE];
    
    NSDictionary *designToPost = [self makeDictionaryforCurrentDesign];
    UIImage *imageToUpload = [imageView imageForState:UIControlStateNormal];
    CGDataProviderRef provider = CGImageGetDataProvider(imageToUpload.CGImage);
    
    NSData* data;
    if ([[designToPost valueForKey:@"type"] isEqualToString:@"Joint Photographic Experts Group (JPEG)"])
        data = UIImageJPEGRepresentation(imageToUpload,0.0);
    else if ([[designToPost valueForKey:@"type"] isEqualToString:@"Portable Network Graphic (PNG)"])
        data = UIImagePNGRepresentation(imageToUpload);
    else
        data = (__bridge NSData*)CGDataProviderCopyData(provider);
    
    WebServiceManager *WS = [[WebServiceManager alloc] init];
    [WS uploadProject:designToPost andDesign:data];
}

-(IBAction)onCancel:(id)sender
{
    [allDesignsImageArray removeAllObjects];
    [allDesignsDetailArray removeAllObjects];
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)onlink:(id)sender
{
    if(allDesignsDetailArray.count == 1)
    {
        UIAlertView* dialog = [[UIAlertView alloc] init];
        [dialog setDelegate:self];
        [dialog setTitle:@"Link Failed"];
        [dialog setMessage:@"Not enough designs to enable link"];
        [dialog addButtonWithTitle:@"Just Show Preview"];
        [dialog addButtonWithTitle:@"Add more designs"];
        [dialog addButtonWithTitle:@"Cancel"];
        dialog.tag = 4;
        
        [dialog show];
    }
    else
    {
        mPreviewLinkViewController.previewOnly = FALSE;
        [self onFullScreen:nil];
    }
}

-(IBAction)ontrash:(id)sender
{
    if(allDesignsDetailArray.count == 1)
    {
        [self onCancel:nil];
    }
    else
    {
        [allDesignsImageArray removeObject:imageToUpdate];
        [allDesignsDetailArray removeObject:imageData];
        
        imageToUpdate = [allDesignsImageArray lastObject];
        imageData = [allDesignsDetailArray lastObject];
        
        [allDesignsCollection reloadData];
        [self loadViewWithImage:imageToUpdate andData:imageData];
    }
}

-(IBAction)onFullScreen:(id)sender
{
    if(!mPreviewLinkViewController.toDisplayImage)
        mPreviewLinkViewController.toDisplayImage = [[UIImage alloc] init];
    mPreviewLinkViewController.toDisplayImage = imageToUpdate;
    
    if(!mPreviewLinkViewController.displayImageDetails)
        mPreviewLinkViewController.displayImageDetails = [[NSMutableDictionary alloc] init];
    [mPreviewLinkViewController.displayImageDetails removeAllObjects];
    [mPreviewLinkViewController.displayImageDetails addEntriesFromDictionary:imageData];
    
    if(!mPreviewLinkViewController.allDesignImages)
        mPreviewLinkViewController.allDesignImages = [[NSMutableArray alloc] init];
    mPreviewLinkViewController.allDesignImages = allDesignsImageArray;
    
    [self presentViewController:mPreviewLinkViewController animated:YES completion:nil];
}

-(void)loadViewWithImage:(UIImage *)selectedImage andData:(NSDictionary *)imageDict
{
    [imageView setImage:selectedImage forState:UIControlStateNormal];
    CGFloat height = selectedImage.size.height;
    CGFloat width = selectedImage.size.width;
    [imageResolution setText:[NSString stringWithFormat:@"%.0fx%.0f",width,height]];
    NSString *designedFor = [self getDesignedForValueForImageSize:selectedImage.size];
    [imageFor setText:designedFor];
    
    [author setText:[[NSUserDefaults standardUserDefaults] objectForKey:@"Default_Author"]];
    [email setText:[[NSUserDefaults standardUserDefaults] objectForKey:@"Default_Email"]];
    
    NSString *Imgsource = [imageDict valueForKey:@"source"];
    
    if ([Imgsource isEqualToString:@"camera_roll"])
    {
        source.text = @"Album";
        sourcePath.hidden = TRUE;
        
        NSURL *imagePath = [imageDict objectForKey:@"UIImagePickerControllerReferenceURL"];
        ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *imageAsset)
        {
            ALAssetRepresentation *imageRep = [imageAsset defaultRepresentation];
            [self.navigationController setTitle:[imageRep filename]];
            [imageName setText:[imageRep filename]];
            [imageSize setText:[NSString stringWithFormat:@"%lld bytes", [imageRep size]]];
            NSString *kind = [self getImageKindforImage:[imageRep filename]];
            [imageType setText:kind];
            
            NSString *orien = @"Unknown";
            switch ([imageRep orientation]) {
                case 0:
                    orien = @"OrientationUp";
                    break;
                case 1:
                    orien = @"OrientationDown";
                    break;
                case 2:
                    orien = @"OrientationLeft";
                    break;
                case 3:
                    orien = @"OrientationRight";
                    break;
                case 4:
                    orien = @"OrientationUpMirrored";
                    break;
                case 5:
                    orien = @"OrientationDownMirrored";
                    break;
                case 6:
                    orien = @"OrientationLeftMirrored";
                    break;
                case 7:
                    orien = @"OrientationRightMirrored";
                    break;
                default:
                    orien = @"Unknown";
                    break;
            }
            [orientation setText:orien];
        };
        
        // get the asset library and fetch the asset based on the ref url (pass in block above)
        ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
        [assetslibrary assetForURL:imagePath resultBlock:resultblock failureBlock:nil];
    }
    else if([Imgsource isEqualToString:@"documents_directory"])
    {
        source.text = @"Documents Directory";
        sourcePath.hidden = TRUE;
        
        [imageName setText:[imageDict valueForKey:@"file_path"]];
        [self.navigationController setTitle:imageName.text];
        NSString *kind = [self getImageKindforImage:imageName.text];
        [imageType setText:kind];
        NSString *size = [ NSString stringWithFormat:@"%@ bytes",[imageDict valueForKey:NSFileSize]];
        [imageSize setText:size];
        NSString *orien = @"Unknown";
        [orientation setText:orien];
    }
    else if([Imgsource isEqualToString:@"url"])
    {
        source.text = @"URL";
        sourcePath.hidden = FALSE;
        NSString *ImgURL = [imageDict valueForKey:@"image_url"];
        sourcePath.text = ImgURL;
        
        [imageName setText:[imageDict valueForKey:@"file_path"]];
        [self.navigationController setTitle:imageName.text];
        NSString *kind = [self getImageKindforImage:imageName.text];
        [imageType setText:kind];
        NSString *size = [ NSString stringWithFormat:@"%@ bytes",[imageDict valueForKey:NSFileSize]];
        [imageSize setText:size];
        NSString *orien = @"Unknown";
        [orientation setText:orien];
    }
    
}

-(NSString *)getDesignedForValueForImageSize:(CGSize)size
{
    
    if ((size.width == 320 && size.height == 480) || (size.width == 480 && size.height == 320))
        return @"iPhone/iPod Touch 3.5inch device";
    else if ((size.width == 640 && size.height == 960) || (size.width == 960 && size.height == 640))
        return @"iPhone/iPod Touch 3.5inch retina device";
    else if ((size.width == 640 && size.height == 1136) || (size.width == 1136 && size.height == 640))
        return @"iPhone/iPod Touch 4inch retina device";
    if ((size.width == 320 && size.height == 568) || (size.width == 568 && size.height == 320))
        return @"iPhone/iPod Touch 4inch device";
    else if ((size.width == 768 && size.height == 1024) || (size.width == 1024 && size.height == 768))
        return @"iPad/iPad Mini device ";
    else if ((size.width == 1536 && size.height == 2048) || (size.width == 2048 && size.height == 1536))
        return @"iPad/iPad Mini retina device ";
    
    return @"Unknown";
}

-(NSString *)getImageKindforImage:(NSString *)imgName
{
    NSArray *items = [imgName componentsSeparatedByString:@"."];
    
    if ([items count] > 0)
    {
        NSString *kind = [[items objectAtIndex:1] uppercaseString];
        if ([kind isEqualToString:@"TIFF"] || [kind isEqualToString:@"TIF"])
            return @"Tagged Image File Format (TIFF)";
        else if ([kind isEqualToString:@"JPG"] || [kind isEqualToString:@"JEPG"])
            return @"Joint Photographic Experts Group (JPEG)";
        else if ([kind isEqualToString:@"GIF"])
            return @"Graphic Interchange Format (GIF)";
        else if ([kind isEqualToString:@"PNG"])
            return @"Portable Network Graphic (PNG)";
        else if ([kind isEqualToString:@"BMP"] || [kind isEqualToString:@"BMPF"])
            return @"Windows Bitmap Format (DIB)";
        else if ([kind isEqualToString:@"ICO"])
            return @"Windows Icon Format";
        else if ([kind isEqualToString:@"CUR"])
            return @"Windows Cursor";
        else if ([kind isEqualToString:@"CBM"])
            return @"X Window System bitmap";
    }
    return @"Unknown";
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    textField.text = @"";
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark -
#pragma mark UploadStatusDelegate

- (void)didFinishUploading
{
    [self.navigationController performSelector:@selector(popToRootViewControllerAnimated:) withObject:[NSNumber numberWithBool:TRUE] afterDelay:0.5];
}

#pragma mark -
#pragma mark UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *) actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            //Camera Roll
            [self getPhoto];
            break;
        case 1:
            //Documents Directory
            [self getDocuments];
            break;
        case 2:
            //URL
            [self showImageURLFiled];
            break;
    }
    [actionSheet dismissWithClickedButtonIndex:3 animated:YES];
}

-(void) getPhoto
{
	if (!picker)
    {
        picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    }
    [self presentViewController:picker animated:TRUE completion:nil];
}

-(void)getDocuments
{
    NSString *documentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDir error:nil];
    NSArray *extensions = [NSArray arrayWithObjects:@"cmb", @"cur", @"ico", @"bmp", @"bmpf", @"png",@"gif", @"jpg",@"jpeg",@"tif", @"tiff", nil];
    
    NSArray *files = [dirContents filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"pathExtension IN %@", extensions]];
    
    if (! mDocumentsDirectoryViewController.allDocumentImageNames)
        mDocumentsDirectoryViewController.allDocumentImageNames = [[NSMutableArray alloc] init];
    mDocumentsDirectoryViewController.delegate = self;
    
    [mDocumentsDirectoryViewController.allDocumentImageNames removeAllObjects];
    [mDocumentsDirectoryViewController.allDocumentImageNames addObjectsFromArray:files];
    
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromTop;
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:mDocumentsDirectoryViewController animated:YES];
    
    //NSPredicate *imagePredicate = [NSPredicate predicateWithFormat:@"self ENDSWITH %@", extensions];
    //NSArray *onlyPics = [dirContents filteredArrayUsingPredicate:imagePredicate];
}

-(void)showImageURLFiled
{
    UIAlertView* dialog = [[UIAlertView alloc] init];
    [dialog setDelegate:self];
    [dialog setTitle:@"Enter design URL"];
    [dialog setMessage:nil];
    [dialog addButtonWithTitle:@"Cancel"];
    [dialog addButtonWithTitle:@"OK"];
    dialog.tag = 5;
    
    dialog.alertViewStyle = UIAlertViewStylePlainTextInput;
    [dialog textFieldAtIndex:0].keyboardType = UIKeyboardTypeURL;
    
    [dialog textFieldAtIndex:0].text = @"http://www.hdwallpapersinhd.net/wp-content/uploads/2013/10/HD-Wallpapers-Desktop-Car-Background-HD-Wallpapers.jpg";
    
    CGAffineTransform moveUp = CGAffineTransformMakeTranslation(0.0, 0.0);
    [dialog setTransform: moveUp];
    [dialog show];
}

#pragma mark -
#pragma mark DocumentsDirectoryDelegate
- (void)didSelectItem:(NSDictionary *)itemDictionary
{
    UIImage *selecteImage = [itemDictionary objectForKey:@"Document_Image"] ;
    NSMutableDictionary *imageDict = [NSMutableDictionary dictionary];
    [imageDict removeAllObjects];
    [imageDict setDictionary:itemDictionary];
    
    [allDesignsImageArray addObject:selecteImage];
    [allDesignsDetailArray addObject:imageDict];
    
    [allDesignsCollection reloadData];
    
    imageToUpdate = [allDesignsImageArray lastObject];
    imageData = [allDesignsDetailArray lastObject];
    [self loadViewWithImage:imageToUpdate andData:imageData];
    
    //    if (! mUploadDetailsViewController.imageToUpdate)
    //        mUploadDetailsViewController.imageToUpdate = [[UIImage alloc] init];
    //    mUploadDetailsViewController.imageToUpdate = selecteImage;
    //
    //    if (! mUploadDetailsViewController.imageData)
    //        mUploadDetailsViewController.imageData = [[NSDictionary alloc] init];
    //    mUploadDetailsViewController.imageData = imageDict;
}

#pragma mark - UIImagePickerController delegate functions

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [info setValue:@"camera_roll" forKey:@"source"];
    
    UIImage *imageToSave, *editedImage, *originalImage;
    
    editedImage = (UIImage*)[info objectForKey:
                             UIImagePickerControllerEditedImage];
    originalImage = (UIImage*)[info objectForKey:
                               UIImagePickerControllerOriginalImage];
    
    if(editedImage)
        imageToSave = editedImage;
    else
        imageToSave = originalImage;
    
    if(imageToSave == nil)
    {
        NSLog(@"Something went wrong. Try again.");
        
        [self dismissViewControllerAnimated:TRUE completion:^{
        }];
        
        return;
    }
    
    UIImage *selecteImage = imageToSave ;
    NSMutableDictionary *imageDict = [NSMutableDictionary dictionary];
    [imageDict removeAllObjects];
    [imageDict setDictionary:info];
    
    [allDesignsImageArray addObject:selecteImage];
    [allDesignsDetailArray addObject:imageDict];
    
    [allDesignsCollection reloadData];
    
    //    if (! mUploadDetailsViewController.imageToUpdate)
    //        mUploadDetailsViewController.imageToUpdate = [[UIImage alloc] init];
    //    mUploadDetailsViewController.imageToUpdate = selecteImage;
    //
    //    if (! mUploadDetailsViewController.imageData)
    //        mUploadDetailsViewController.imageData = [[NSDictionary alloc] init];
    //    mUploadDetailsViewController.imageData = imageDict;
    
    [self dismissViewControllerAnimated:TRUE completion:^{
        imageToUpdate = [allDesignsImageArray lastObject];
        imageData = [allDesignsDetailArray lastObject];
        [self loadViewWithImage:imageToUpdate andData:imageData];
    }];
}

#pragma mark -
#pragma mark UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 5 && buttonIndex == 1)
    {
        [self.view setUserInteractionEnabled:FALSE];
        [cloudView setFrame:CGRectMake(10, 124, 300, 320)];
        cloudView.layer.cornerRadius = 15;
        cloudView.layer.borderWidth = 0.5;
        cloudView.layer.borderColor = [UIColor whiteColor].CGColor;
        [cloudView setupViewforUpload:@"image"];
        
        [self.view addSubview:cloudView];
        
        NSString *designURL = [alertView textFieldAtIndex:0].text;
        NSLog(@"%@",designURL);
        WebServiceManager *WS = [[WebServiceManager alloc] init];
        [WS downloadImageforURL:designURL];
        
    }
    if (alertView.tag == 4 && buttonIndex == 1)
    {
        [uploadOptionSheet showInView:self.view];
    }
    if (alertView.tag == 4 && buttonIndex == 0)
    {
        mPreviewLinkViewController.previewOnly = TRUE;
        [self onFullScreen:nil];
    }
}

#pragma mark -
#pragma mark UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return ([allDesignsImageArray count] + 1);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"designCollectionCell" forIndexPath:indexPath];
    
    NSArray *viewsToRemove = [cell.contentView subviews];
    for (UIView *v in viewsToRemove)
        [v removeFromSuperview];
    
    
    UIImageView *designImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 70, 70)];
    designImage.contentMode = UIViewContentModeScaleAspectFit;
    
    if (indexPath.item == allDesignsImageArray.count)
    {
        [designImage setImage:[UIImage imageNamed:@"ico_add"]];
        [designImage setFrame:CGRectMake(17.5, 17.5, 35, 35)];
    }
    else
    {
        [designImage setImage:[allDesignsImageArray objectAtIndex:indexPath.item]];
        [designImage setFrame:CGRectMake(0, 0, 70, 70)];
    }
    
    [cell.contentView addSubview:designImage];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.item == allDesignsImageArray.count)
    {
        [uploadOptionSheet showInView:self.view];
    }
    else
    {
        imageToUpdate = [allDesignsImageArray objectAtIndex:indexPath.item];
        imageData = [allDesignsDetailArray objectAtIndex:indexPath.item];
        [self loadViewWithImage:imageToUpdate andData:imageData];
    }
}


@end
