//
//  ViewController.m
//  DesignShare
//
//  Created by Bastin Raj on 11/8/13.
//  Copyright (c) 2013 Pramati. All rights reserved.
//

#import "PreviewViewController.h"
#import "DesignGestureRecognizer.h"
#import <QuartzCore/QuartzCore.h>
#import "FeedbackViewController.h"
#import <UIKit/UIGestureRecognizerSubclass.h>
#import "XMPPConnection.h"
#import "AFHTTPClient.h"
#import "AFHTTPRequestOperation.h"
#import "CloseImageView.h"
#import "BorderView.h"
#import "FeedbackCountLabel.h"

@implementation PreviewViewController

@synthesize scrollView;
@synthesize imageView;
@synthesize startAreaPoint;
@synthesize popoverController;
@synthesize titlebar;
@synthesize designArray;
@synthesize designAuthor;
@synthesize authorEmail;

#define extraWidth 5
#define extraHeight 5
#define drawingAreaMinWidth 30
#define drawingAreaMinHeight 30

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view, typically from a nib.
    [self.navigationController setNavigationBarHidden: YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = TRUE;
    [self.titlebar setHidden: YES];
    [self.view setUserInteractionEnabled: YES];
    [self initialize];
    [self loadFeedbackImage];
    [self connectXmppServer];
}



-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear: YES];
    [self disConnectXmppServer];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView: touch.view];
    CGPoint pointInView =  [touch locationInView: self.imageView];
    
    NSDictionary *requiredPoint = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithFloat:point.x], @"x",[NSNumber numberWithFloat:point.y],@"y", nil];
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    
    [self performSelector:@selector(longTap:) withObject:requiredPoint afterDelay:1.2];
    
    UIView *drawnView = [self viewDrawnAtThisPoint: pointInView];
    
    [self resetValues];
    
    if ( drawnView == nil )  // No view present in the selected area
    {
        [self drawFeedbackViewRectangle: point];
    }
    else // Some drawn view present in the selected area
    {
        CGPoint feedbackTouchPoint =  [touch locationInView: drawnView];
        
        // Check whether remove icon tapped or not
        if ( [self isRemoveIconTapped: drawnView tappedPoint: feedbackTouchPoint])
        {
            [self removeDrawnFeedbackView: drawnView];
        }
        else
        {
            [self makeViewSelection: drawnView];
            [self unSelectAlreadySelectedView];
        }
    }
}



-(BOOL) isRemoveIconTapped: (UIView *) feedbackView tappedPoint: (CGPoint) point
{
    BOOL status = FALSE;
    CGRect closeImageViewRect;
    
    for(UIView *currentView in feedbackView.subviews)
    {
        if ( [currentView isKindOfClass: [CloseImageView class] ])
        {
            closeImageViewRect = currentView.frame;
            break;
        }
    }
    
    
    if ( CGRectContainsPoint(closeImageViewRect, point))
    {
        status = TRUE;
    }
    
    return status;
}


- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView: touch.view];
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    
    [self resizeFeedbackViewRectangle: point];
}


- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView: touch.view];
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    
    [self completeFeedbackViewDrawing: point];
}


- (void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

-(void) longTap:(NSDictionary *)point
{
    CGPoint requiredPoint;
    requiredPoint.x = [[point valueForKey:@"x"] floatValue];
    requiredPoint.y = [[point valueForKey:@"y"] floatValue];
    
    [self completeFeedbackPointDrawing:requiredPoint];
    NSLog(@"handle long tap..");
}

/*
 Method to initialize data and set view title
 */
-(void) initialize
{
    // initialize feedback array
    feedbacks = [[NSMutableDictionary alloc] init];
    
    // Set view title
    self.title = @"";
    
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showHideNavigation:)];
    doubleTap.numberOfTapsRequired = 2;
    [self.view addGestureRecognizer: doubleTap];
    [self.view bringSubviewToFront:self.titlebar];
}

-(void) showHideNavigation: (UITapGestureRecognizer *) gesture
{
    [self.view bringSubviewToFront:self.titlebar];
    if (self.titlebar.hidden)
        [self.titlebar setHidden: NO];
    else
        [self.titlebar setHidden: YES];
}

/*
 Method to load uploaded image or image to write feedback
 */
-(void) loadFeedbackImage
{
    UIImage *uploadedImage = [UIImage imageNamed:@"is.png"];
    self.imageView = [[UIImageView alloc] initWithImage: uploadedImage];
    
    // self.imageView = [[UIImageView alloc] initWithImage: uploadedImage];
    self.imageView = [[UIImageView alloc] init];
    
    self.imageView.contentMode = UIViewContentModeScaleToFill;
    self.imageView.userInteractionEnabled = YES;
    self.imageView.frame = self.scrollView.frame;
    
    [self.view addSubview: self.imageView];
}


-(void) drawFeedbackViewRectangle: (CGPoint) point
{
    startAreaPoint = CGPointMake(point.x-extraWidth, point.y-extraHeight);
    
    CGRect rectSize = CGRectMake(startAreaPoint.x, startAreaPoint.y, 2, 2);   // Initial starting point
    
    UIView *feedbackRectView = [[UIView alloc] initWithFrame:rectSize];
    BorderView *borderView = [[BorderView alloc] initWithFrame: CGRectMake(5, 5, 2, 2)];
    
    [borderView.layer setBorderColor: [UIColor redColor].CGColor];
    [borderView.layer setBorderWidth: 1.0];
    
    feedbackRectView.tag = feedbacks.count + 1;
    borderView.tag = feedbackRectView.tag;
    
    [feedbackRectView addSubview: borderView];
    [self.imageView addSubview: feedbackRectView];
    
    currentDrawnView = feedbackRectView;
}


-(void) resizeFeedbackViewRectangle: (CGPoint) point
{
    // Updated 14th Nov 2013 after 3 PM
    CGSize currentViewSize = CGSizeMake(point.x - startAreaPoint.x+extraWidth, point.y - startAreaPoint.y);
    UIView *feedbackView = (UIView *) currentDrawnView;
    feedbackView.frame = CGRectMake(startAreaPoint.x, startAreaPoint.y, currentViewSize.width, currentViewSize.height);

    UIView *borderView = nil;
    for ( UIView *currentView in feedbackView.subviews)
    {
        if ( [currentView isKindOfClass: [BorderView class]] )
        {
            borderView = currentView;
            break;
        }
    }
    
    CGRect borderViewSize = CGRectMake(borderView.frame.origin.x, borderView.frame.origin.y, feedbackView.frame.size.width- extraWidth*2, feedbackView.frame.size.height-extraHeight);
    
    borderView.frame = borderViewSize;
}


/* Method to complete feedback view. This method get called when user releases mouse
 */
-(void) completeFeedbackViewDrawing: (CGPoint) point
{
    UIView *feedbackView = (UIView *) currentDrawnView;
    if (feedbackView.frame.size.height > drawingAreaMinHeight && feedbackView.frame.size.width > drawingAreaMinWidth)
    {
        [self setViewSelectionColor:  feedbackView];
        [self addFeedbackViewComponents: feedbackView endPoint: point];
        
        // Show feedback popover view to get user feedback text
        [self showFeedbackPopoverView: feedbackView withFeedbackText: nil];
    }
    else
    {
        NSLog(@"Drawn size is small. Ignoring drawn rect...");
        [feedbackView removeFromSuperview];
        
    }
}

-(void) completeFeedbackPointDrawing: (CGPoint) point
{
    [self showFeedbackPopoverAtPoint:point withFeedbackText:nil];
    
    UIView *currentView = (UIView *) currentDrawnView;
    [self addFeedbackPointWithTagNumber:currentView.tag endPoint:point];
}


/*
 Method to remove already drawn feedback view
 */
-(void) removeDrawnFeedbackView: (UIView *) drawnView
{
    NSInteger feedbackCount = drawnView.tag;
    [drawnView removeFromSuperview];

    // Removed from saved dictionary
    if (feedbacks != nil && [feedbacks objectForKey: [NSNumber numberWithInteger: feedbackCount]] != nil )
    {
        [feedbacks removeObjectForKey: [NSNumber numberWithInteger: feedbackCount]];

        [self reArrangeViews: feedbackCount];
        [self rearrangeFeedback: feedbackCount];
    }
}


/*
 Method to add feedback view components such as rectangle main view, round image, count label and close image
 */
-(void) addFeedbackViewComponents: (UIView *) feedbackView endPoint: (CGPoint) point
{
    // Place count icon
    UIImage *countImage = [UIImage imageNamed:@"round.png"];
    UIImageView *countImageView = [[UIImageView alloc] initWithImage: countImage];
    countImageView.frame = CGRectMake(-2,-2, countImage.size.width, countImage.size.height);
    
    [feedbackView addSubview: countImageView];
    
    // Place count label
    FeedbackCountLabel *countLabel = [[FeedbackCountLabel alloc] initWithFrame:CGRectMake(0, 0, 15, 14)];

    [countLabel setText: [NSString stringWithFormat:@"%li", feedbacks.count+1]];
    [countLabel setBackgroundColor:[UIColor clearColor]];
    [countLabel setTextColor: [UIColor whiteColor]];
    [countLabel setFont: [UIFont fontWithName:@"Trebuchet MS" size:12]];
    countLabel.textAlignment = NSTextAlignmentCenter;
    
    [feedbackView addSubview: countLabel];
    
    // Place delete icon
    UIImage *closeImage = [UIImage imageNamed:@"delete.png"];
    CloseImageView *removeImageView = [[CloseImageView alloc] initWithImage: closeImage];
    removeImageView.userInteractionEnabled = YES;
    removeImageView.frame = CGRectMake(feedbackView.frame.size.width+4-closeImage.size.width,  -4, closeImage.size.width, closeImage.size.height);

    [feedbackView addSubview: removeImageView];
}


-(void) addFeedbackPointWithTagNumber:(NSInteger)tagValue endPoint:(CGPoint)point
{
    tagValue = tagValue + 1;
    
    // Place count icon
    UIImage *countImage = [UIImage imageNamed:@"round.png"];
    UIImageView *countImageView = [[UIImageView alloc] initWithImage: countImage];
    
    tagValue = tagValue + 1;
    countImageView.tag = tagValue;
    
    countImageView.center = point;
    
    [self.imageView addSubview: countImageView];
    
    // Place count label
    UILabel *countLabel = [[UILabel alloc] init];
    countLabel.frame = countImageView.frame;
    
    tagValue = tagValue + 1;
    countLabel.tag = tagValue;
    
    [countLabel setText: [NSString stringWithFormat:@"%lu", (unsigned long)feedbacks.count+1]];
    [countLabel setBackgroundColor:[UIColor clearColor]];
    [countLabel setTextColor: [UIColor whiteColor]];
    [countLabel setFont: [UIFont fontWithName:@"Trebuchet MS" size:12]];
    countLabel.textAlignment = NSTextAlignmentCenter;
    
    [self.imageView addSubview: countLabel];

    //UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(feedbackRemoveIconTapped:)];
    //tapGesture.numberOfTapsRequired = 2;
    //tapGesture.cancelsTouchesInView = YES;
    //[countImageView addGestureRecognizer: tapGesture];
    //[countLabel addGestureRecognizer: tapGesture];

    //closeImageTapGestureRecognizer = tapGesture;

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(feedbackRemoveIconTapped:)];
    tapGesture.numberOfTapsRequired = 2;
    [countImageView addGestureRecognizer: tapGesture];
    [countLabel addGestureRecognizer: tapGesture];
}


-(void) rearrangeFeedback: (NSInteger) feedbackCount
{
    // Change feedback index for the stored text
    NSMutableArray *changeRequiredKeys = [[NSMutableArray alloc] init];
    for( NSString *str in feedbacks )
    {
        NSInteger storedCount = [str integerValue];
        if (storedCount > feedbackCount)
        {
            [changeRequiredKeys addObject: [NSString stringWithFormat:@"%ld", (long)storedCount]];
        }
    }
    
    NSDictionary *temp = [feedbacks copy];

    // Now process all changes required keys
    for (NSString *key in changeRequiredKeys)
    {
        int count = [key intValue];
        int requiredCount = count - 1;

        if ( [feedbacks objectForKey: [NSNumber numberWithInt: count]] != nil )
        {
            [feedbacks removeObjectForKey: [NSNumber numberWithInt: count]];
        }
        
        if ( [feedbacks objectForKey: [NSNumber numberWithInt: requiredCount]] != nil )
        {
            [feedbacks removeObjectForKey: [NSNumber numberWithInt: requiredCount]];
        }
        
        // Add changed index to feedback dictionary
        [feedbacks setObject:[temp objectForKey: [NSNumber numberWithInt: count]] forKey: [NSNumber numberWithInt: requiredCount]];
    }
}


/*
 Method to rearrange views
 */
-(void) reArrangeViews: (NSInteger) tagValue
{
    for (UIView *view in self.imageView.subviews)
    {
        if (view.tag > tagValue)
        {
            NSInteger newTag = view.tag - 1;
            view.tag = newTag;
            
            for (UIView *subView in view.subviews)
            {
                if ([subView isKindOfClass: [FeedbackCountLabel class]])
                {
                    FeedbackCountLabel *label = (FeedbackCountLabel *) subView;
                    label.text = [NSString stringWithFormat:@"%ld", newTag];
                    break;
                }
            }
        }
    }
}


/*
 Method to show feedback text view as popover
 */
-(void) showFeedbackPopoverView: (UIView *) feedbackView withFeedbackText: (NSString *) enteredText
{
    feedbackController = [[FeedbackViewController alloc] initWithStyle: UITableViewStylePlain withFeedbackText: enteredText];
    feedbackController.delegate = self;
    //[feedbackController viewLoaded];
    
    self.popoverController = [[WEPopoverController alloc] initWithContentViewController: feedbackController];
    
    CGRect rect = CGRectMake(feedbackView.frame.origin.x, feedbackView.frame.origin.y, 10, 10);
    [self.popoverController presentPopoverFromRect: rect inView:self.imageView permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    self.popoverController.delegate = self;
}

-(void) showFeedbackPopoverAtPoint:(CGPoint)point withFeedbackText:(NSString *)enteredText
{
    feedbackController = [[FeedbackViewController alloc] initWithStyle: UITableViewStylePlain withFeedbackText: enteredText];
    feedbackController.delegate = self;
    //[feedbackController viewLoaded];
    
    self.popoverController = [[WEPopoverController alloc] initWithContentViewController: feedbackController];
    
    CGRect rect = CGRectMake(point.x, point.y, 10, 10);
    [self.popoverController presentPopoverFromRect: rect inView:self.imageView permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    self.popoverController.delegate = self;
}

-(void) keyboardAppeared: (NSNotification *) notification
{
    //NSDictionary *info  = [notification userInfo];
    //CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    [UIView animateWithDuration:0.3 animations:^{
        
        self.imageView.frame = CGRectMake(self.imageView.frame.origin.x, self.imageView.frame.origin.y-150, self.imageView.frame.size.width, self.view.frame.size.height);
        CGRect newRect = CGRectMake( self.popoverController.view.frame.origin.x,  self.popoverController.view.frame.origin.y-150, self.popoverController.view.frame.size.width, self.popoverController.view.frame.size.height);
        
        self.popoverController.view.frame = newRect;
    }];
    
}

-(void) keyboardDisappeared: (NSNotification *) notification
{
    [self.popoverController dismissPopoverAnimated: YES];
    //NSDictionary *info  = [notification userInfo];
    //CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.imageView.frame = CGRectMake(self.imageView.frame.origin.x, 0, self.imageView.frame.size.width, self.imageView.frame.size.height);
    }];
}


// Delegate method which gets called when user entered feedback text and clicked save button
-(void) userDidEnterFeedback: (NSString *) feedbackText
{
    [self.popoverController dismissPopoverAnimated: YES];
    
    float x = 0;
    float y = 0;
    float width = 0;
    float height = 0;
    
    if ([feedbackText isEqualToString:@""])
    {
        UIView *drawnView = (UIView *) currentDrawnView;
        [self removeDrawnFeedbackView: drawnView];
    }
    else
    {
        NSInteger feedbackCount = 0;
        if (currentDrawnView != nil )
        {
            UIView *feedbackView = (UIView *) currentDrawnView;
            feedbackCount = feedbackView.tag;
            
            x = feedbackView.frame.origin.x;
            y = feedbackView.frame.origin.y;
            width = feedbackView.frame.size.width;
            height = feedbackView.frame.size.height;
        }
        else if (currentSelectedView != nil )
        {
            feedbackCount = currentSelectedView.tag;
            
            UIView *feedbackView = (UIView *) currentSelectedView;
            x = feedbackView.frame.origin.x;
            y = feedbackView.frame.origin.y;
            width = feedbackView.frame.size.width;
            height = feedbackView.frame.size.height;
        }

        // Save user entered feedback in dictionary
        [self saveFeedbackText:feedbackText forFeedbackCount: feedbackCount];
        
        // Send user feedback text and drawn frame co-ordinates to other user
        [connection sendMessage: feedbackText toUser: nil xPosition: x yPosition: y  widthValue:width heightValue:height];
    }
}


/*
 Method gets called when used clicked cancel button in feedback popover view
 */
-(void) feedbackCancelButtonClicked
{
    if (self.popoverController.isPopoverVisible)
    {
        [self.popoverController dismissPopoverAnimated: YES];
        
        UIView *drawnView = (UIView *) currentDrawnView;
        [self removeDrawnFeedbackView: drawnView];
    }
}


#pragma mark - WEPopoverControllerDelegate methods
- (BOOL)popoverControllerShouldDismissPopover:(WEPopoverController *)popoverController
{
    return  YES;
}


- (void)popoverControllerDidDismissPopover:(WEPopoverController *)popoverController
{
    // Get feedback text. If user entered something show action sheet to save or ignore already entered message
    if ([feedbackController.feedbackTextView.text isEqualToString:@""])
    {
        [self.popoverController dismissPopoverAnimated: YES];

        UIView *drawnView = (UIView *) currentDrawnView;
        [self removeDrawnFeedbackView: drawnView];
    }
    else
    {
        UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"You have entered feedback" delegate:self cancelButtonTitle:@"Save" destructiveButtonTitle:@"Ignore" otherButtonTitles:nil, nil];
        [sheet showInView:self.view];
    }
}


#pragma mark - UIActionSheetDelegate methods
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)  // Ignore button clicked
    {
        //[self feedbackRemoveIconTapped: closeImageTapGestureRecognizer];
        UIView *drawnView = (UIView *) currentDrawnView;
        [self removeDrawnFeedbackView: drawnView];
    }
    else // Save button clicked
    {
        NSInteger feedbackCount = 0;
        if (currentDrawnView != nil )
        {
            UIView *fView = (UIView *) currentDrawnView;
            feedbackCount = fView.tag;
        }
        else if (currentSelectedView != nil )
        {
            feedbackCount = currentSelectedView.tag;
        }
        
        [self saveFeedbackText: feedbackController.feedbackTextView.text forFeedbackCount: feedbackCount];
    }
}

-(void) saveFeedbackText: (NSString *) feedbackText forFeedbackCount: (NSInteger) feedbackCount
{
    // For update check if already exists. If exists remove and add it again since it is dictionary
    if ( [feedbacks objectForKey: [NSNumber numberWithInteger: feedbackCount]] != nil )
    {
        [feedbacks removeObjectForKey:[NSNumber numberWithInteger: feedbackCount]];
    }
    
    [feedbacks setObject:feedbackText forKey: [NSNumber numberWithInteger: feedbackCount]];
}



/*
 Method to check is there view already drawn in this point or not.
 */
-(UIView *) viewDrawnAtThisPoint: (CGPoint ) point
{
    UIView *drawnView = nil;
    for ( UIView *currentView in self.imageView.subviews)
    {
        if (CGRectContainsPoint(currentView.frame, point))
        {
            drawnView = currentView;
            break;
        }
    }
    return  drawnView;
}

/*
 Method which sets view background and alpha color. So that it appears like view selected
 */
-(void) makeViewSelection: (UIView *) drawnView
{
    [self setViewSelectionColor: drawnView];
    currentSelectedView = drawnView;
    
    // Get feedback text and show that text in popover view
    [self showFeedbackPopoverView: drawnView withFeedbackText: [feedbacks objectForKey: [NSNumber numberWithInteger: drawnView.tag]]];
}

/*
 Method which sets view background and alpha color. So that it appears like view not selected
 */
-(void) makeViewUnSelection: (UIView *) drawnView
{
    drawnView.backgroundColor = [UIColor clearColor];
    drawnView.alpha = 1;
    drawnView.layer.borderColor = [UIColor redColor].CGColor;
}


/*
 Method to set color for the selected view
 */
-(void) setViewSelectionColor: (UIView *) drawnView
{
    for (UIView *currentView in  drawnView.subviews)
    {
        if ([currentView isKindOfClass: [BorderView class] ])
        {
            currentView.backgroundColor = [UIColor blackColor];
            currentView.alpha = 0.3;
            currentView.layer.borderColor = [UIColor redColor].CGColor;
            break;
        }
    }
}


/*
 This method resets already stored values
 */
-(void) resetValues
{
    startAreaPoint = CGPointMake(0, 0);
    currentDrawnView = nil;
}


-(void) unSelectAlreadySelectedView
{
    for ( UIView *currentView in self.imageView.subviews)
    {
        if ( ![currentView isEqual: currentSelectedView] &&currentView.backgroundColor == [UIColor clearColor] && currentView.alpha == 1)
        {
            [self makeViewUnSelection: currentView];
            break;
        }
    }
}

/*
 Method to exit current screen
 */
-(IBAction) logoutAction:(id)sender
{
    if (feedbackController != nil )
    {
        [feedbackController.feedbackTextView resignFirstResponder];
    }
    
    [self.navigationController popViewControllerAnimated: YES];
}

-(void) connectXmppServer
{
    connection = [[XMPPConnection alloc] init];
    connection.delegate = self;
    [connection connect];
}

-(void) disConnectXmppServer
{
    if (connection != nil )
    {
        [connection disconnect];
    }
}


/*
 This method gets called when user receives feedback from someone
 */
-(void) feedbackReceived: (NSString *) feedbackText xPosition: (float) x yPosition: (float) y  widthValue: (float) width  heightValue: (float) height
{
    // If feedback view is already drawn view then show feedback message in thread
    NSLog(@"Inside feedbackReceived..");
    CGRect frameRect = CGRectMake(x, y,  width, height);
    UIView *feedbackView = [[UIView alloc] initWithFrame:frameRect];
    
    feedbackView.backgroundColor = [UIColor blackColor];
    feedbackView.alpha = 0.3;
    feedbackView.layer.borderColor = [UIColor redColor].CGColor;
    
    [self.imageView addSubview: feedbackView];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Feedback Message " message:feedbackText delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
    [alert show];
}

-(void)setupImage:(NSMutableDictionary *)dict
{
    [designArray removeAllObjects];
    
    designAuthor = [dict valueForKey:@"author_name"];
    authorEmail = [dict valueForKey:@"author_email"];
    NSLog(@"design Array : %@",[[dict valueForKey:@"designs"] class]);
    designArray = [[dict valueForKey:@"designs"] mutableCopy];
    if ([designArray count] <= 0)
        [self.navigationController popViewControllerAnimated: YES];
    
    [self loadDesignAtIndex:1];
}

-(void)loadDesignAtIndex:(int)index
{
    if ([designArray count] > 0)
    {
        NSDictionary *requiredDict = nil;
        for (NSDictionary *searchDict in designArray)
        {
            if ([[searchDict valueForKey:@"order_number"] intValue] == index)
            {
                requiredDict = [searchDict mutableCopy];
                break;
            }
        }
        if (requiredDict)
        {
            NSURL *imageUrl = [[NSURL alloc] initWithString:[requiredDict valueForKey:@"image_url"]];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^(void)
                           {
                               NSData *data0 = [NSData dataWithContentsOfURL:imageUrl];
                               UIImage *image = [UIImage imageWithData:data0];
                               NSDictionary *nowDict = [requiredDict mutableCopy];
                               dispatch_sync(dispatch_get_main_queue(), ^(void)
                                             {
                                                 if (!self.imageView)
                                                     self.imageView = [[UIImageView alloc] init];
                                                 
                                                 self.imageView.image = image;
                                                 
                                                 NSArray *viewsToRemove = [self.imageView subviews];
                                                 
                                                 for (UIView *v in viewsToRemove)
                                                     [v removeFromSuperview];

                                                 [self drawTouchPointsforDesignforDictionart:nowDict];
                                             });
                           });
        }
    }
}

-(void)drawTouchPointsforDesignforDictionart:(NSDictionary *)currectDesignDict
{
    NSArray *touchViews = [currectDesignDict valueForKey:@"touch_potistions"];
    for (NSDictionary *points in touchViews)
    {
        NSDictionary *position = [points valueForKey:@"position"];
        CGRect rect;
        rect.origin.x = [[position valueForKey:@"x"] intValue] ;
        rect.origin.y = [[position valueForKey:@"y"] intValue] ;
        rect.size.width = [[position valueForKey:@"width"] intValue] ;
        rect.size.height = [[position valueForKey:@"height"] intValue] ;
        
        UIButton *touchButton = [[UIButton alloc] initWithFrame:rect];
        touchButton.layer.borderWidth = 0.3;
        touchButton.layer.cornerRadius = 3;
        touchButton.layer.borderColor = [UIColor blackColor].CGColor;
        [touchButton setBackgroundColor:[UIColor greenColor]];
        [touchButton setAlpha:0.3];
        [touchButton setShowsTouchWhenHighlighted:YES];
        [touchButton addTarget:self
                        action:@selector(touchMethod:)
              forControlEvents:UIControlEventTouchDown];
        touchButton.tag = [[points valueForKey:@"destination_order"] intValue];
        [self.imageView addSubview:touchButton];
        [self.imageView bringSubviewToFront:touchButton];
    }
}

-(void)touchMethod:(UIButton *)sender
{
    int destinationImage = sender.tag;
    [self loadDesignAtIndex:destinationImage];
}


@end
