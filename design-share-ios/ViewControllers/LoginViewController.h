//
//  LoginViewController.h
//  Design Share
//
//  Created by Gautham on 25/10/13.
//  Copyright (c) 2013 Pramati. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UploadDetailsViewController.h"
#import "DocumentsDirectoryViewController.h"

@interface LoginViewController : UIViewController<UINavigationControllerDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate, DocumentsDirectoryDelegate,UploadStatusDelegate, UIAlertViewDelegate>
{
    UIActionSheet *uploadOptionSheet;
    UIImage *selecteImage;
    NSMutableDictionary *imageDict;
}

@property(nonatomic) UIActionSheet *uploadOptionSheet;
@property(nonatomic) IBOutlet UITextField *fakeField;
@property(nonatomic) IBOutlet UITextField *field1;
@property(nonatomic) IBOutlet UITextField *field2;
@property(nonatomic) IBOutlet UITextField *field3;
@property(nonatomic) IBOutlet UITextField *field4;
@property(nonatomic) IBOutlet UITextField *field5;

@property(nonatomic) IBOutlet UIButton *uploadDesignButton;

-(IBAction)onUploadDesign:(id)sender;

@end
