//
//  PreviewLinkViewController.m
//  design-share-ios
//
//  Created by Gautham on 10/12/13.
//  Copyright (c) 2013 Pramati. All rights reserved.
//

#import "PreviewLinkViewController.h"

@interface PreviewLinkViewController ()

@end

@implementation PreviewLinkViewController
@synthesize toDisplayImage,displayImageView,allDesignImages;
@synthesize allDesignDetails,displayImageDetails;
@synthesize baseScrollView,allDesignsCollection;
@synthesize previewOnly,backButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [allDesignsCollection reloadData];
    [self loadViewWithImage:toDisplayImage andData:displayImageDetails];
}

-(BOOL)prefersStatusBarHidden
{
    return YES;
}

-(IBAction)onBack:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)loadViewWithImage:(UIImage *)selectedImage andData:(NSDictionary *)imageDict
{
    /*
     [displayImageView removeFromSuperview];
     //[baseScrollView setFrame:self.view.frame];
     if (selectedImage.size.width <= baseScrollView.frame.size.width && selectedImage.size.height <= baseScrollView.frame.size.height)
     {
     [baseScrollView setContentSize:CGSizeMake(baseScrollView.frame.size.width, baseScrollView.frame.size.height)];
     CGRect imageFrame = CGRectMake(0, 0, selectedImage.size.width, selectedImage.size.height);
     imageFrame.origin.x = (self.view.frame.size.width - selectedImage.size.width)/2;
     imageFrame.origin.y = (self.view.frame.size.height - selectedImage.size.height)/2;
     
     [displayImageView setFrame:imageFrame];
     }
     else
     {
     //        [baseScrollView setContentSize:selectedImage.size];
     //        [displayImageView setFrame:CGRectMake(baseScrollView.frame.origin.x, baseScrollView.frame.origin.y, baseScrollView.contentSize.width, baseScrollView.contentSize.height)];
     [displayImageView setFrame:baseScrollView.frame];
     
     }
     */
    
    [displayImageView removeFromSuperview];
    [baseScrollView setContentSize:CGSizeMake(baseScrollView.frame.size.width, baseScrollView.frame.size.height)];
    [displayImageView setFrame:baseScrollView.frame];
    
    [displayImageView setImage:selectedImage];
    [baseScrollView addSubview:displayImageView];
}

#pragma mark -
#pragma mark UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return (allDesignImages.count + 1);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"alldesignPreviewCell" forIndexPath:indexPath];
    
    NSArray *viewsToRemove = [cell.contentView subviews];
    for (UIView *v in viewsToRemove)
        [v removeFromSuperview];
    
    UIImageView *designImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 70, 70)];
    designImage.contentMode = UIViewContentModeScaleAspectFit;
    
    [designImage setFrame:CGRectMake(0, 0, 70, 70)];
    
    if (indexPath.item == 0)
        [designImage setImage:[UIImage imageNamed:@"ico_exit.png"]];
    else
        [designImage setImage:[allDesignImages objectAtIndex:(indexPath.item - 1)]];
    
    [cell.contentView addSubview:designImage];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.item == 0)
    {
        [self onBack:nil];
    }
    else
    {
        toDisplayImage = [allDesignImages objectAtIndex:(indexPath.item - 1)];
        displayImageDetails = [allDesignDetails objectAtIndex:(indexPath.item - 1)];
        [self loadViewWithImage:toDisplayImage andData:displayImageDetails];
    }
}
@end
