//
//  UploadStatusViewController.h
//  design-share-ios
//
//  Created by Gautham on 13/11/13.
//  Copyright (c) 2013 Pramati. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol UploadStatusDelegate;

@interface UploadStatusViewController : UIViewController
{
    IBOutlet UIImageView *statusImage;
    IBOutlet UILabel *statusString;
    IBOutlet UILabel *designerlbl;
    IBOutlet UILabel *designerPin;
    IBOutlet UILabel *reviewerlbl;
    IBOutlet UILabel *reviewerPin;
    IBOutlet UIBarButtonItem *shareButton;
    NSString *status;
    NSMutableDictionary *responseObject;

}

@property (nonatomic, weak) id<UploadStatusDelegate> delegate;

@property(nonatomic, strong) UIImageView *statusImage;
@property(nonatomic, strong) UILabel *statusString;
@property(nonatomic, strong) UILabel *designerlbl;
@property(nonatomic, strong) UILabel *designerPin;
@property(nonatomic, strong) UILabel *reviewerPin;
@property(nonatomic, strong) UILabel *reviewerlbl;
@property(nonatomic, strong) UIBarButtonItem *shareButton;
@property(nonatomic) NSString *status;
@property(nonatomic) NSMutableDictionary *responseObject;

-(IBAction)onDone:(id)sender;
-(IBAction)onShare:(id)sender;

-(void) updateStatus:(NSString *)succ withResponse:(NSDictionary *)dict;

@end

@protocol UploadStatusDelegate <NSObject>
@optional

- (void)didFinishUploading;

@end