//
//  LoginViewController.m
//  Design Share
//
//  Created by Gautham on 25/10/13.
//  Copyright (c) 2013 Pramati. All rights reserved.
//

#import "LoginViewController.h"
#import "DocumentsDirectoryViewController.h"
#import "WebServiceManager.h"
#import "CloudView.h"
#import "PreviewViewController.h"

@interface LoginViewController ()
{
    UIImagePickerController * picker;
    UploadDetailsViewController *mUploadDetailsViewController;
    DocumentsDirectoryViewController *mDocumentsDirectoryViewController;
    BOOL viewPopped;
    CloudView *cloudView;
    UploadStatusViewController *mUploadStatusViewController;
    PreviewViewController *mPreViewController;
}

@end

@implementation LoginViewController

@synthesize uploadOptionSheet;

@synthesize fakeField;
@synthesize field1,field2,field3,field4,field5;
@synthesize uploadDesignButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    self.navigationController.navigationBar.translucent = NO;
    
    if (!selecteImage)
        selecteImage = [[UIImage alloc] init];
    
    if (!imageDict)
        imageDict = [[NSMutableDictionary alloc] init];
    
    uploadOptionSheet = [[UIActionSheet alloc] initWithTitle:@"Upload from " delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera Roll", @"Documents Directory",@"URL", nil];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle: nil];
    mUploadDetailsViewController = [storyboard instantiateViewControllerWithIdentifier:@"UploadDetailsViewController"];
    mUploadDetailsViewController.navigationItem.hidesBackButton = YES;
    
    mDocumentsDirectoryViewController =[storyboard instantiateViewControllerWithIdentifier:@"DocumentsDirectoryViewController"];
    mDocumentsDirectoryViewController.navigationItem.hidesBackButton = YES;
    
    mUploadStatusViewController = [storyboard instantiateViewControllerWithIdentifier:@"UploadStatusViewController"];
    mUploadStatusViewController.navigationItem.hidesBackButton = YES;
    mUploadStatusViewController.delegate = self;
    
    mPreViewController = [storyboard instantiateViewControllerWithIdentifier:@"PreViewController"];
    mPreViewController.navigationItem.hidesBackButton = YES;

    uploadDesignButton.layer.cornerRadius = 5;
    uploadDesignButton.layer.borderWidth = 1;
    uploadDesignButton.layer.borderColor = [[UIColor blackColor] CGColor];
    viewPopped = FALSE;
    
    cloudView = [CloudView cloudView];
    [cloudView setupViewforUpload:@"download"];
	// Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localNotificationhandler:) name:NOTIFICATION_DOWNLOAD_DATA_FAILED object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localNotificationhandler:) name:NOTIFICATION_DOWNLOAD_DATA_SUCCESS object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localNotificationhandler:) name:NOTIFICATION_DID_DOWNLOAD_IMAGE object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localNotificationhandler:) name:NOTIFICATION_FAIL_DOWNLOAD_IMAGE object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden: NO];

    [super viewWillAppear:animated];
    [fakeField setText:@""];
    [field1 setText:@""];
    [field2 setText:@""];
    [field3 setText:@""];
    [field4 setText:@""];
    [field5 setText:@""];
    [field5 setAlpha:0.2];
    [fakeField becomeFirstResponder];
}

- (void) localNotificationhandler:(NSNotification *) notification
{
    [self.view setUserInteractionEnabled:TRUE];
    if ([[notification name] isEqualToString:NOTIFICATION_DOWNLOAD_DATA_FAILED])
    {
        [cloudView removeFromSuperview];
        if (!(mUploadStatusViewController.status))
            mUploadStatusViewController.status = [[NSString alloc] init];
        
        mUploadStatusViewController.status = NOTIFICATION_DOWNLOAD_DATA_FAILED;
        [self presentViewController:mUploadStatusViewController animated:TRUE completion:^{
            [mUploadStatusViewController updateStatus:NOTIFICATION_DOWNLOAD_DATA_FAILED withResponse:nil];
        }];
        
    }
    else if ([[notification name] isEqualToString:NOTIFICATION_DOWNLOAD_DATA_SUCCESS])
    {
        [cloudView removeFromSuperview];
        if (!mPreViewController.designArray)
            mPreViewController.designArray = [[NSMutableArray alloc] init];
        [mPreViewController.designArray removeAllObjects];
        
        [mPreViewController setupImage:[notification object]];
        [self.navigationController pushViewController:mPreViewController animated:YES];
        
        //
        //        mUploadStatusViewController.success = TRUE;
        //        [self presentViewController:mUploadStatusViewController animated:TRUE completion:^{
        //            [mUploadStatusViewController updateStatus:NOTIFICATION_DOWNLOAD_DATA_SUCCESS];
        //        }];
    }
    else if([[notification name] isEqualToString:NOTIFICATION_DID_DOWNLOAD_IMAGE])
    {
        [self.view setUserInteractionEnabled:TRUE];
        [cloudView removeFromSuperview];
        
        NSDictionary *notDict = [notification object];
        selecteImage = [notDict objectForKey:@"Document_Image"];;
        
        if (! mUploadDetailsViewController.imageToUpdate)
            mUploadDetailsViewController.imageToUpdate = [[UIImage alloc] init];
        mUploadDetailsViewController.imageToUpdate = selecteImage;
        
        [imageDict removeAllObjects];
        [imageDict setDictionary:notDict];
        
        if (! mUploadDetailsViewController.imageData)
            mUploadDetailsViewController.imageData = [[NSDictionary alloc] init];
        mUploadDetailsViewController.imageData = imageDict;
        
        [self.navigationController pushViewController:mUploadDetailsViewController animated:YES];
    }
    else if ([[notification name] isEqualToString:NOTIFICATION_FAIL_DOWNLOAD_IMAGE])
    {
        [self.view setUserInteractionEnabled:TRUE];
        
        [cloudView removeFromSuperview];
        if (!(mUploadStatusViewController.status))
            mUploadStatusViewController.status = [[NSString alloc] init];
        
        mUploadStatusViewController.status = NOTIFICATION_FAIL_DOWNLOAD_IMAGE;
        [self presentViewController:mUploadStatusViewController animated:TRUE completion:^{
            [mUploadStatusViewController updateStatus:NOTIFICATION_FAIL_DOWNLOAD_IMAGE withResponse:nil];
        }];
        
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    if (viewPopped)
    {
        viewPopped = FALSE;
        [self.navigationController pushViewController:mUploadDetailsViewController animated:YES];
    }
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

-(IBAction)onUploadDesign:(id)sender
{
    if ([fakeField isFirstResponder])
        [fakeField resignFirstResponder];
    
    [uploadOptionSheet showInView:self.view];
}

-(void) getPhoto
{
	if (!picker)
    {
        picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    }
    [self presentViewController:picker animated:TRUE completion:nil];
}

-(void)getDocuments
{
    NSString *documentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDir error:nil];
    NSArray *extensions = [NSArray arrayWithObjects:@"cmb", @"cur", @"ico", @"bmp", @"bmpf", @"png",@"gif", @"jpg",@"jpeg",@"tif", @"tiff", nil];
    
    NSArray *files = [dirContents filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"pathExtension IN %@", extensions]];
    
    if (! mDocumentsDirectoryViewController.allDocumentImageNames)
        mDocumentsDirectoryViewController.allDocumentImageNames = [[NSMutableArray alloc] init];
    mDocumentsDirectoryViewController.delegate = self;
    
    [mDocumentsDirectoryViewController.allDocumentImageNames removeAllObjects];
    [mDocumentsDirectoryViewController.allDocumentImageNames addObjectsFromArray:files];
    
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromTop;
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:mDocumentsDirectoryViewController animated:YES];
    
    //NSPredicate *imagePredicate = [NSPredicate predicateWithFormat:@"self ENDSWITH %@", extensions];
    //NSArray *onlyPics = [dirContents filteredArrayUsingPredicate:imagePredicate];
}

-(void)showImageURLFiled
{
    UIAlertView* dialog = [[UIAlertView alloc] init];
    [dialog setDelegate:self];
    [dialog setTitle:@"Enter design URL"];
    [dialog setMessage:nil];
    [dialog addButtonWithTitle:@"Cancel"];
    [dialog addButtonWithTitle:@"OK"];
    dialog.tag = 5;
    
    dialog.alertViewStyle = UIAlertViewStylePlainTextInput;
    [dialog textFieldAtIndex:0].keyboardType = UIKeyboardTypeURL;
    
    [dialog textFieldAtIndex:0].text = @"http://www.hdwallpapersinhd.net/wp-content/uploads/2013/10/HD-Wallpapers-Desktop-Car-Background-HD-Wallpapers.jpg";
    
    CGAffineTransform moveUp = CGAffineTransformMakeTranslation(0.0, 0.0);
    [dialog setTransform: moveUp];
    [dialog show];
}

-(void)doValidation:(NSString *)pin
{
    [self.view setUserInteractionEnabled:FALSE];
    [cloudView setFrame:CGRectMake(10, 124, 300, 320)];
    cloudView.layer.cornerRadius = 15;
    cloudView.layer.borderWidth = 0.5;
    cloudView.layer.borderColor = [UIColor whiteColor].CGColor;
    
    [cloudView setupViewforUpload:@"upload"];
    
    [self.view addSubview:cloudView];
    
    WebServiceManager *WS = [[WebServiceManager alloc] init];
    [WS loginwithPin:pin];
}

#pragma mark -
#pragma mark UITextFieldDelagate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self doValidation:textField.text];
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *completeString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    switch ([completeString length]) {
        case 5:
            [field5 setAlpha:1.0];
            [field1 setText:@"*"];
            [field2 setText:@"*"];
            [field3 setText:@"*"];
            [field4 setText:@"*"];
            [field5 setText:@"*"];
            [self doValidation:completeString];
            break;
        case 4:
            [field5 setAlpha:0.2];
            [field1 setText:@"*"];
            [field2 setText:@"*"];
            [field3 setText:@"*"];
            [field4 setText:@"*"];
            [field5 setText:@""];
            break;
        case 3:
            [field5 setAlpha:0.2];
            [field1 setText:@"*"];
            [field2 setText:@"*"];
            [field3 setText:@"*"];
            [field4 setText:@""];
            [field5 setText:@""];
            break;
        case 2:
            [field5 setAlpha:0.2];
            [field1 setText:@"*"];
            [field2 setText:@"*"];
            [field3 setText:@""];
            [field4 setText:@""];
            [field5 setText:@""];
            break;
        case 1:
            [field5 setAlpha:0.2];
            [field1 setText:@"*"];
            [field2 setText:@""];
            [field3 setText:@""];
            [field4 setText:@""];
            [field5 setText:@""];
            break;
        case 0:
            [field5 setAlpha:0.2];
            [field1 setText:@""];
            [field2 setText:@""];
            [field3 setText:@""];
            [field4 setText:@""];
            [field5 setText:@""];
            break;
        default:
            break;
    }
    return YES;
}

#pragma mark -
#pragma mark UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *) actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            //Camera Roll
            [self getPhoto];
            break;
        case 1:
            //Documents Directory
            [self getDocuments];
            break;
        case 2:
            //URL
            [self showImageURLFiled];
            break;
    }
    [fakeField becomeFirstResponder];
    [actionSheet dismissWithClickedButtonIndex:3 animated:YES];
}

#pragma mark -
#pragma mark UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 5 && buttonIndex == 1)
    {
        [self.view setUserInteractionEnabled:FALSE];
        [cloudView setFrame:CGRectMake(10, 124, 300, 320)];
        cloudView.layer.cornerRadius = 15;
        cloudView.layer.borderWidth = 0.5;
        cloudView.layer.borderColor = [UIColor whiteColor].CGColor;
        [cloudView setupViewforUpload:@"image"];
        
        [self.view addSubview:cloudView];
        
        NSString *designURL = [alertView textFieldAtIndex:0].text;
        NSLog(@"%@",designURL);
        WebServiceManager *WS = [[WebServiceManager alloc] init];
        [WS downloadImageforURL:designURL];
        
    }
}

#pragma mark - UIImagePickerController delegate functions

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [info setValue:@"camera_roll" forKey:@"source"];
    
    UIImage *imageToSave, *editedImage, *originalImage;
    
    editedImage = (UIImage*)[info objectForKey:
                             UIImagePickerControllerEditedImage];
    originalImage = (UIImage*)[info objectForKey:
                               UIImagePickerControllerOriginalImage];
    
    if(editedImage)
        imageToSave = editedImage;
    else
        imageToSave = originalImage;
    
    if(imageToSave == nil)
    {
        NSLog(@"Something went wrong. Try again.");
        
        [self dismissViewControllerAnimated:TRUE completion:^{
            [fakeField becomeFirstResponder];
        }];
        
        return;
    }
    
    selecteImage = imageToSave ;
    [imageDict removeAllObjects];
    [imageDict setDictionary:info];
    
    if (! mUploadDetailsViewController.imageToUpdate)
        mUploadDetailsViewController.imageToUpdate = [[UIImage alloc] init];
    mUploadDetailsViewController.imageToUpdate = selecteImage;
    
    if (! mUploadDetailsViewController.imageData)
        mUploadDetailsViewController.imageData = [[NSDictionary alloc] init];
    mUploadDetailsViewController.imageData = imageDict;
    
    [self dismissViewControllerAnimated:TRUE completion:^{
        [self.navigationController pushViewController:mUploadDetailsViewController animated:YES];
    }];
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:TRUE completion:^{
        [fakeField becomeFirstResponder];
    }];
}

#pragma mark -
#pragma mark DocumentsDirectoryDelegate
- (void)didSelectItem:(NSDictionary *)itemDictionary
{
    viewPopped = TRUE;
    
    selecteImage = [itemDictionary objectForKey:@"Document_Image"] ;
    [imageDict removeAllObjects];
    [imageDict setDictionary:itemDictionary];
    
    if (! mUploadDetailsViewController.imageToUpdate)
        mUploadDetailsViewController.imageToUpdate = [[UIImage alloc] init];
    mUploadDetailsViewController.imageToUpdate = selecteImage;
    
    if (! mUploadDetailsViewController.imageData)
        mUploadDetailsViewController.imageData = [[NSDictionary alloc] init];
    mUploadDetailsViewController.imageData = imageDict;
}

#pragma mark -
#pragma mark UploadStatusDelegate

- (void)didFinishUploading
{
    [fakeField becomeFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
