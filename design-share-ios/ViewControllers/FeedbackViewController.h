//
//  FeedbackViewController.h
//  DesignShare
//
//  Created by Bastin Raj on 11/18/13.
//  Copyright (c) 2013 Pramati. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PreviewControllerDelegate.h"

@interface FeedbackViewController : UITableViewController
{
    UITextView *feedbackTextView;
    NSString *alreadyEnteredFeedbackText;
}


@property(nonatomic, retain) id<PreviewControllerDelegate> delegate;
@property(nonatomic, retain) UITextView *feedbackTextView;

-(IBAction) submitButtonAction: (id)sender;

- (id)initWithStyle:(UITableViewStyle) style withFeedbackText: (NSString *) enteredText;

@end
