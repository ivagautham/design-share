//
//  ViewController.m
//  DesignShare
//
//  Created by Bastin Raj on 11/8/13.
//  Copyright (c) 2013 Pramati. All rights reserved.
//

#import "PreviewViewController.h"
#import "DesignGestureRecognizer.h"
#import <QuartzCore/QuartzCore.h>
#import "FeedbackViewController.h"
#import <UIKit/UIGestureRecognizerSubclass.h>
#import "XMPPConnection.h"
#import "AFHTTPClient.h"
#import "AFHTTPRequestOperation.h"

@implementation PreviewViewController

@synthesize scrollView;
@synthesize imageView;
@synthesize startAreaPoint;
@synthesize popoverController;
@synthesize titlebar;
@synthesize designArray;
@synthesize designAuthor;
@synthesize authorEmail;

#define extraWidth 10
#define extraHeight 10
#define drawingAreaMinWidth 30
#define drawingAreaMinHeight 30
#define MAX_COMPONENT 4

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view, typically from a nib.
    [self.navigationController setNavigationBarHidden: YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = TRUE;
    [self.titlebar setHidden: YES];
    [self.view setUserInteractionEnabled: YES];
    [self initialize];
    [self loadFeedbackImage];
    [self connectXmppServer];
}



-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear: YES];
    [self disConnectXmppServer];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView: touch.view];
    CGPoint pointInView =  [touch locationInView: self.imageView];
    
    NSDictionary *requiredPoint = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithFloat:point.x], @"x",[NSNumber numberWithFloat:point.y],@"y", nil];
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self performSelector:@selector(longTap:) withObject:requiredPoint afterDelay:1.2];
    
    UIView *drawnView = [self viewDrawnAtThisPoint: pointInView];
    
    [self resetValues];
    
    if ( drawnView == nil )  // No view present in the selected area
    {
        startAreaPoint = point;
        [self drawFeedbackViewRectangle: point];
    }
    else // Some drawn view present in the selected area
    {
        [self makeViewSelection: drawnView];
        [self unSelectAlreadySelectedView];
    }
}


- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView: touch.view];
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    
    [self resizeFeedbackViewRectangle: point];
}


- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView: touch.view];
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    
    [self completeFeedbackViewDrawing: point];
}


- (void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

-(void) longTap:(NSDictionary *)point
{
    CGPoint requiredPoint;
    requiredPoint.x = [[point valueForKey:@"x"] floatValue];
    requiredPoint.y = [[point valueForKey:@"y"] floatValue];
    
    [self completeFeedbackPointDrawing:requiredPoint];
    NSLog(@"handle long tap..");
}

/*
 Method to initialize data and set view title
 */
-(void) initialize
{
    // initialize feedback array
    feedbacks = [[NSMutableDictionary alloc] init];
    
    // Set view title
    self.title = @"";
    
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showHideNavigation:)];
    doubleTap.numberOfTapsRequired = 2;
    [self.view addGestureRecognizer: doubleTap];
    [self.view bringSubviewToFront:self.titlebar];
}

-(void) showHideNavigation: (UITapGestureRecognizer *) gesture
{
    [self.view bringSubviewToFront:self.titlebar];
    if (self.titlebar.hidden)
        [self.titlebar setHidden: NO];
    else
        [self.titlebar setHidden: YES];
}

/*
 Method to load uploaded image or image to write feedback
 */
-(void) loadFeedbackImage
{
    //UIImage *uploadedImage = [UIImage imageNamed:@"/Users/bastins/Desktop/champagne.png"];
    UIImage *uploadedImage = [UIImage imageNamed:@"is.png"];
    
    // self.imageView = [[UIImageView alloc] initWithImage: uploadedImage];
    self.imageView = [[UIImageView alloc] init];
    
    self.imageView.contentMode = UIViewContentModeScaleToFill;
    
    self.imageView.userInteractionEnabled = YES;
    
    self.imageView.frame = self.scrollView.frame;
    
    //self.scrollView.contentSize = self.imageView.bounds.size;
    //self.scrollView.contentMode = UIViewContentModeScaleToFill;
    
    //[self.scrollView addSubview: self.imageView];
    
    
    
    [self.view addSubview: self.imageView];
    
    /*
     if (self.imageView.frame.size.width <= 320)
     {
     self.imageView.center = self.scrollView.center;
     }
     */
    
    //DesignGestureRecognizer *recognizer = [[DesignGestureRecognizer alloc] init];
    //[self.imageView addGestureRecognizer: recognizer];
    //recognizer.callBackDelegate = self;
    
    //self.scrollView.contentSize = self.imageView.bounds.size;
    //self.scrollView.contentMode = UIViewContentModeScaleAspectFit;
}


-(void) drawFeedbackViewRectangle: (CGPoint) endAreaPoint
{
    // Updated 14th Nov 2013 after 3 PM
    CGSize startSize = CGSizeMake(2, 2);
    CGRect frameRect = CGRectMake(startAreaPoint.x, startAreaPoint.y,  startSize.width, startSize.height);
    UIView *feedbackView = [[UIView alloc] initWithFrame:frameRect];
    
    [feedbackView setBackgroundColor: [UIColor clearColor]];
    [feedbackView.layer setBorderColor:[UIColor redColor].CGColor];
    [feedbackView.layer setBorderWidth: 1.0];
    feedbackView.layer.cornerRadius = 3.0;
    
    [self.imageView addSubview: feedbackView];
    currentDrawnView = feedbackView;
}


-(void) resizeFeedbackViewRectangle: (CGPoint) point
{
    // Updated 14th Nov 2013 after 3 PM
    CGSize currentViewSize = CGSizeMake(point.x - startAreaPoint.x, point.y - startAreaPoint.y);
    UIView *feedbackView = (UIView *) currentDrawnView;
    feedbackView.frame = CGRectMake(startAreaPoint.x, startAreaPoint.y, currentViewSize.width, currentViewSize.height);
}


/* Method to complete feedback view. This method get called when user releases mouse
 */
-(void) completeFeedbackViewDrawing: (CGPoint) point
{
    UIView *feedbackView = (UIView *) currentDrawnView;
    if (feedbackView.frame.size.height > drawingAreaMinHeight && feedbackView.frame.size.width > drawingAreaMinWidth)
    {
        feedbackView.backgroundColor = [UIColor blackColor];
        feedbackView.alpha = 0.3;
        feedbackView.layer.borderColor = [UIColor redColor].CGColor;
        
        [self showFeedbackPopoverView: feedbackView withFeedbackText: nil];
        
        NSInteger tagValue = 0;
        if (feedbacks.count == 0) // There is no feedback at all. User first time entered text
        {
            tagValue = tagValue * MAX_COMPONENT;
        }
        else
        {
            tagValue = feedbacks.count; // Get previous view tag value and plus one for current drawing view
            tagValue = tagValue * MAX_COMPONENT;
        }
        
        [self addFeedbackViewComponents: feedbackView tagNumber: tagValue endPoint: point];
    }
    else
    {
        NSLog(@"Drawn size is small. Ignoring drawn rect...");
        [feedbackView removeFromSuperview];
        
    }
}

-(void) completeFeedbackPointDrawing: (CGPoint) point
{
    
    [self showFeedbackPopoverAtPoint:point withFeedbackText:nil];
    
    NSInteger tagValue = 0;
    if (feedbacks.count == 0) // There is no feedback at all. User first time entered text
    {
        tagValue = tagValue * MAX_COMPONENT;
    }
    else
    {
        tagValue = feedbacks.count; // Get previous view tag value and plus one for current drawing view
        tagValue = tagValue * MAX_COMPONENT;
    }
    
    [self addFeedbackPointWithTagNumber:tagValue endPoint:point];
}

/*
 Method to delete view when close icon tapped
 */
-(void) feedbackRemoveIconTapped: (UITapGestureRecognizer *) gesture
{
    if (self.popoverController.isPopoverVisible)
    {
        [self.popoverController dismissPopoverAnimated: YES];
    }
    
    UIImageView *closeImageView = (UIImageView *) gesture.view;
    
    NSInteger tagValue = closeImageView.tag / MAX_COMPONENT;
    NSInteger feedbackCount = tagValue;
    
    tagValue = tagValue - 1; // Get previous
    tagValue = tagValue * MAX_COMPONENT;
    
    // Remove closed image respective views
    for (int index =1; index <= MAX_COMPONENT; index++)
    {
        NSInteger tagToRemove = tagValue + index;
        UIView *viewToRemove = (UIView *) [self.imageView viewWithTag: tagToRemove];
        [viewToRemove removeFromSuperview];
    }
    
    // Removed from saved dictionary
    if (feedbacks != nil && [feedbacks objectForKey: [NSNumber numberWithInteger: feedbackCount]] != nil )
    {
        [self reArrangeViews: feedbackCount];
        [feedbacks removeObjectForKey: [NSNumber numberWithInteger: feedbackCount]];
        [self rearrangeFeedback: feedbackCount];
    }
}


/*
 Method to add feedback view components such as rectangle main view, round image, count label and close image
 */
-(void) addFeedbackViewComponents: (UIView *) feedbackView tagNumber: (NSInteger) tagValue endPoint: (CGPoint) point
{
    tagValue = tagValue + 1;
    feedbackView.tag = tagValue;
    
    // Place count icon
    UIImage *countImage = [UIImage imageNamed:@"round.png"];
    UIImageView *countImageView = [[UIImageView alloc] initWithImage: countImage];
    
    tagValue = tagValue + 1;
    countImageView.tag = tagValue;
    
    countImageView.frame = CGRectMake(feedbackView.frame.origin.x-6,feedbackView.frame.origin.y-8, countImage.size.width, countImage.size.height);
    
    [self.imageView addSubview: countImageView];
    
    // Place count label
    UILabel *countLabel = [[UILabel alloc] initWithFrame:CGRectMake(feedbackView.frame.origin.x-3, feedbackView.frame.origin.y-5, 15, 14)];
    
    tagValue = tagValue + 1;
    countLabel.tag = tagValue;
    
    [countLabel setText: [NSString stringWithFormat:@"%lu", (unsigned long)feedbacks.count+1]];
    [countLabel setBackgroundColor:[UIColor clearColor]];
    [countLabel setTextColor: [UIColor whiteColor]];
    [countLabel setFont: [UIFont fontWithName:@"Trebuchet MS" size:12]];
    countLabel.textAlignment = NSTextAlignmentCenter;
    [self.imageView addSubview: countLabel];
    
    // Place delete icon
    UIImage *closeImage = [UIImage imageNamed:@"delete.png"];
    UIImageView *removeImageView = [[UIImageView alloc] initWithImage: closeImage];
    removeImageView.userInteractionEnabled = YES;
    
    tagValue = tagValue + 1;
    removeImageView.tag = tagValue;
    
    removeImageView.frame = CGRectMake(point.x-15,feedbackView.frame.origin.y-10, closeImage.size.width, closeImage.size.height);
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(feedbackRemoveIconTapped:)];
    [removeImageView addGestureRecognizer: tapGesture];
    
    closeImageTapGestureRecognizer = tapGesture;
    
    [self.imageView addSubview: removeImageView];
}

-(void) addFeedbackPointWithTagNumber:(NSInteger)tagValue endPoint:(CGPoint)point
{
    tagValue = tagValue + 1;
    
    // Place count icon
    UIImage *countImage = [UIImage imageNamed:@"round.png"];
    UIImageView *countImageView = [[UIImageView alloc] initWithImage: countImage];
    
    tagValue = tagValue + 1;
    countImageView.tag = tagValue;
    
    countImageView.center = point;
    
    [self.imageView addSubview: countImageView];
    
    // Place count label
    UILabel *countLabel = [[UILabel alloc] init];
    countLabel.frame = countImageView.frame;
    
    tagValue = tagValue + 1;
    countLabel.tag = tagValue;
    
    [countLabel setText: [NSString stringWithFormat:@"%lu", (unsigned long)feedbacks.count+1]];
    [countLabel setBackgroundColor:[UIColor clearColor]];
    [countLabel setTextColor: [UIColor whiteColor]];
    [countLabel setFont: [UIFont fontWithName:@"Trebuchet MS" size:12]];
    countLabel.textAlignment = NSTextAlignmentCenter;
    
    [self.imageView addSubview: countLabel];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(feedbackRemoveIconTapped:)];
    tapGesture.numberOfTapsRequired = 2;
    [countImageView addGestureRecognizer: tapGesture];
    [countLabel addGestureRecognizer: tapGesture];
    
    closeImageTapGestureRecognizer = tapGesture;
}


-(void) rearrangeFeedback: (NSInteger) feedbackCount
{
    // Change feedback index for the stored text
    NSMutableArray *changeRequiredKeys = [[NSMutableArray alloc] init];
    for( NSString *str in feedbacks )
    {
        NSInteger storedCount = [str integerValue];
        if (storedCount > feedbackCount)
        {
            [changeRequiredKeys addObject: [NSString stringWithFormat:@"%ld", (long)storedCount]];
        }
    }
    
    NSDictionary *temp = [feedbacks copy];
    
    // Now process all changes required keys
    for (NSString *key in changeRequiredKeys)
    {
        int count = [key intValue];
        int requiredCount = count - 1;
        
        if ( [feedbacks objectForKey: [NSNumber numberWithInt: requiredCount]] != nil )
        {
            [feedbacks removeObjectForKey: [NSNumber numberWithInt: requiredCount]];
        }
        
        // Add changed index to feedback dictionary
        [feedbacks setObject:[temp objectForKey: [NSNumber numberWithInt: count]] forKey: [NSNumber numberWithInt: requiredCount]];
    }
    
}


/*
 Method to rearrange views
 */
-(void) reArrangeViews: (NSInteger) tagValue
{
    tagValue = tagValue * MAX_COMPONENT;
    for (UIView *view in self.imageView.subviews)
    {
        if (view.tag > tagValue)
        {
            view.tag = view.tag - MAX_COMPONENT;
            // Update label
            if (view.tag % MAX_COMPONENT == 3) // UILabel component added in third place. After changing position update changed feedback count in UILabel
            {
                UILabel *label = (UILabel *) view;
                label.text = [NSString stringWithFormat:@"%ld", view.tag/MAX_COMPONENT+1];
            }
        }
    }
}

/*
 Method to show feedback text view as popover
 */
-(void) showFeedbackPopoverView: (UIView *) feedbackView withFeedbackText: (NSString *) enteredText
{
    feedbackController = [[FeedbackViewController alloc] initWithStyle: UITableViewStylePlain withFeedbackText: enteredText];
    feedbackController.delegate = self;
    //[feedbackController viewLoaded];
    
    self.popoverController = [[WEPopoverController alloc] initWithContentViewController: feedbackController];
    
    CGRect rect = CGRectMake(feedbackView.frame.origin.x, feedbackView.frame.origin.y, 10, 10);
    [self.popoverController presentPopoverFromRect: rect inView:self.imageView permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    self.popoverController.delegate = self;
}

-(void) showFeedbackPopoverAtPoint:(CGPoint)point withFeedbackText:(NSString *)enteredText
{
    feedbackController = [[FeedbackViewController alloc] initWithStyle: UITableViewStylePlain withFeedbackText: enteredText];
    feedbackController.delegate = self;
    //[feedbackController viewLoaded];
    
    self.popoverController = [[WEPopoverController alloc] initWithContentViewController: feedbackController];
    
    CGRect rect = CGRectMake(point.x, point.y, 10, 10);
    [self.popoverController presentPopoverFromRect: rect inView:self.imageView permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    self.popoverController.delegate = self;
}

-(void) keyboardAppeared: (NSNotification *) notification
{
    //NSDictionary *info  = [notification userInfo];
    //CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    [UIView animateWithDuration:0.3 animations:^{
        
        self.imageView.frame = CGRectMake(self.imageView.frame.origin.x, self.imageView.frame.origin.y-150, self.imageView.frame.size.width, self.view.frame.size.height);
        CGRect newRect = CGRectMake( self.popoverController.view.frame.origin.x,  self.popoverController.view.frame.origin.y-150, self.popoverController.view.frame.size.width, self.popoverController.view.frame.size.height);
        
        self.popoverController.view.frame = newRect;
    }];
    
}

-(void) keyboardDisappeared: (NSNotification *) notification
{
    [self.popoverController dismissPopoverAnimated: YES];
    //NSDictionary *info  = [notification userInfo];
    //CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.imageView.frame = CGRectMake(self.imageView.frame.origin.x, 0, self.imageView.frame.size.width, self.imageView.frame.size.height);
    }];
}

// Delegate method which gets called when user entered feedback text and clicked save button
-(void) userDidEnterFeedback: (NSString *) feedbackText
{
    [self.popoverController dismissPopoverAnimated: YES];
    
    
    float x = 0;
    float y = 0;
    float width = 0;
    float height = 0;
    
    
    if ([feedbackText isEqualToString:@""])
    {
        [self feedbackRemoveIconTapped: closeImageTapGestureRecognizer];
    }
    else
    {
        NSInteger viewTag = 0;
        if (currentDrawnView != nil )
        {
            UIView *fView = (UIView *) currentDrawnView;
            viewTag = fView.tag;
            
            x = fView.frame.origin.x;
            y = fView.frame.origin.y;
            width = fView.frame.size.width;
            height = fView.frame.size.height;
        }
        else if (currentSelectedView != nil )
        {
            viewTag = currentSelectedView.tag;
            
            UIView *fView = (UIView *) currentSelectedView;
            x = fView.frame.origin.x;
            y = fView.frame.origin.y;
            width = fView.frame.size.width;
            height = fView.frame.size.height;
        }
        
        NSInteger feedbackCount = 1;
        if (viewTag > 1)
        {
            feedbackCount = viewTag/MAX_COMPONENT;
            feedbackCount = feedbackCount + 1;
        }
        
        [self saveFeedbackText:feedbackText forFeedbackCount: feedbackCount];
        [connection sendMessage: feedbackText toUser: nil xPosition: x yPosition: y  widthValue:width heightValue:height];
        
    }
}

-(void) feedbackCancelButtonClicked
{
    if (self.popoverController.isPopoverVisible)
    {
        [self.popoverController dismissPopoverAnimated: YES];
        [self feedbackRemoveIconTapped: closeImageTapGestureRecognizer];
    }
}


#pragma mark - WEPopoverControllerDelegate methods
- (BOOL)popoverControllerShouldDismissPopover:(WEPopoverController *)popoverController
{
    return  YES;
}


- (void)popoverControllerDidDismissPopover:(WEPopoverController *)popoverController
{
    // Get feedback text. If user entered something show action sheet to save or ignore already entered message
    if ([feedbackController.feedbackTextView.text isEqualToString:@""])
    {
        [self.popoverController dismissPopoverAnimated: YES];
        [self feedbackRemoveIconTapped: closeImageTapGestureRecognizer];
    }
    else
    {
        UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"You have entered feedback" delegate:self cancelButtonTitle:@"Save" destructiveButtonTitle:@"Ignore" otherButtonTitles:nil, nil];
        [sheet showInView:self.view];
    }
}


#pragma mark - UIActionSheetDelegate methods
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)  // Ignore button clicked
    {
        [self feedbackRemoveIconTapped: closeImageTapGestureRecognizer];
    }
    else // Save button clicked
    {
        NSInteger viewTag = 0;
        if (currentDrawnView != nil )
        {
            UIView *fView = (UIView *) currentDrawnView;
            viewTag = fView.tag;
        }
        else if (currentSelectedView != nil )
        {
            viewTag = currentSelectedView.tag;
        }
        
        NSInteger feedbackCount = 0;
        if (viewTag > 1)
        {
            feedbackCount = viewTag/MAX_COMPONENT;
            feedbackCount = feedbackCount + 1;
        }
        
        [self saveFeedbackText: feedbackController.feedbackTextView.text forFeedbackCount: feedbackCount];
    }
}

-(void) saveFeedbackText: (NSString *) feedbackText forFeedbackCount: (NSInteger) feedbackCount
{
    // For update check if already exists. If exists remove and add it again since it is dictionary
    if ( [feedbacks objectForKey: [NSNumber numberWithInteger: feedbackCount]] != nil )
    {
        [feedbacks removeObjectForKey:[NSNumber numberWithInteger: feedbackCount]];
    }
    
    [feedbacks setObject:feedbackText forKey: [NSNumber numberWithInteger: feedbackCount]];
}


/*
 Method to check is there view already drawn in this point or not.
 */
-(UIView *) viewDrawnAtThisPoint: (CGPoint ) point
{
    UIView *drawnView = nil;
    for ( UIView *currentView in self.imageView.subviews)
    {
        NSInteger viewTag = currentView.tag;
        
        if (viewTag % MAX_COMPONENT == 1 || viewTag == 1)  // Check only for main view. Ignore other views such as image view, label view etc.
        {
            if (CGRectContainsPoint(currentView.frame, point))
            {
                drawnView = currentView;
                break;
            }
        }
    }
    
    return  drawnView;
}

/*
 Method which sets view background and alpha color. So that it appears like view selected
 */
-(void) makeViewSelection: (UIView *) drawnView
{
    drawnView.backgroundColor = [UIColor clearColor];
    drawnView.alpha = 1;
    drawnView.layer.borderColor = [UIColor redColor].CGColor;
    currentSelectedView = drawnView;
    
    // Get feedback text and show that text in popover view
    NSInteger viewTag = drawnView.tag;
    NSInteger index = 1;
    
    if (viewTag > 1)
    {
        index = viewTag / MAX_COMPONENT;
        index = index + 1;  // Get feedback count from drawn view tag
    }
    
    [self showFeedbackPopoverView: drawnView withFeedbackText: [feedbacks objectForKey: [NSNumber numberWithInteger: index]]];
}

/*
 Method which sets view background and alpha color. So that it appears like view not selected
 */
-(void) makeViewUnSelection: (UIView *) drawnView
{
    drawnView.backgroundColor = [UIColor blackColor];
    drawnView.alpha = 0.3;
    drawnView.layer.borderColor = [UIColor redColor].CGColor;
}

/*
 This method resets already stored values
 */
-(void) resetValues
{
    startAreaPoint = CGPointMake(0, 0);
    currentDrawnView = nil;
}


-(void) unSelectAlreadySelectedView
{
    for ( UIView *currentView in self.imageView.subviews)
    {
        NSInteger viewTag = currentView.tag;
        if (viewTag % MAX_COMPONENT == 1 || viewTag == 1)  // Check only for main view. Ignore other views such as image view, label view etc.
        {
            if ( ![currentView isEqual: currentSelectedView] &&currentView.backgroundColor == [UIColor clearColor] && currentView.alpha == 1)
            {
                [self makeViewUnSelection: currentView];
                break;
            }
        }
    }
}

/*
 Method to exit current screen
 */
-(IBAction) logoutAction:(id)sender
{
    if (feedbackController != nil )
    {
        [feedbackController.feedbackTextView resignFirstResponder];
    }
    
    [self.navigationController popViewControllerAnimated: YES];
}

-(void) connectXmppServer
{
    connection = [[XMPPConnection alloc] init];
    connection.delegate = self;
    [connection connect];
}

-(void) disConnectXmppServer
{
    if (connection != nil )
    {
        [connection disconnect];
    }
}


/*
 This method gets called when user receives feedback from someone
 */
-(void) feedbackReceived: (NSString *) feedbackText xPosition: (float) x yPosition: (float) y  widthValue: (float) width  heightValue: (float) height
{
    NSLog(@"Inside feedbackReceived..");
    CGRect frameRect = CGRectMake(x, y,  width, height);
    UIView *feedbackView = [[UIView alloc] initWithFrame:frameRect];
    
    feedbackView.backgroundColor = [UIColor blackColor];
    feedbackView.alpha = 0.3;
    feedbackView.layer.borderColor = [UIColor redColor].CGColor;
    
    [self.imageView addSubview: feedbackView];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Feedback Message " message:feedbackText delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
    [alert show];
}


-(IBAction) menuAction:(id)sender
{
    
}

-(void)setupImage:(NSMutableDictionary *)dict
{
    [designArray removeAllObjects];
    
    designAuthor = [dict valueForKey:@"author_name"];
    authorEmail = [dict valueForKey:@"author_email"];
    NSLog(@"design Array : %@",[[dict valueForKey:@"designs"] class]);
    designArray = [[dict valueForKey:@"designs"] mutableCopy];
    if ([designArray count] <= 0)
        [self.navigationController popViewControllerAnimated: YES];
    
    [self loadDesignAtIndex:1];
}

-(void)loadDesignAtIndex:(int)index
{
    if ([designArray count] > 0)
    {
        NSDictionary *requiredDict = nil;
        for (NSDictionary *searchDict in designArray)
        {
            if ([[searchDict valueForKey:@"order_number"] intValue] == index)
            {
                requiredDict = [searchDict mutableCopy];
                break;
            }
        }
        if (requiredDict)
        {
            NSURL *imageUrl = [[NSURL alloc] initWithString:[requiredDict valueForKey:@"image_url"]];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^(void)
                           {
                               NSData *data0 = [NSData dataWithContentsOfURL:imageUrl];
                               UIImage *image = [UIImage imageWithData:data0];
                               NSDictionary *nowDict = [requiredDict mutableCopy];
                               dispatch_sync(dispatch_get_main_queue(), ^(void)
                                             {
                                                 if (!self.imageView)
                                                     self.imageView = [[UIImageView alloc] init];
                                                 
                                                 self.imageView.image = image;
                                                 
                                                 NSArray *viewsToRemove = [self.imageView subviews];
                                                 
                                                 for (UIView *v in viewsToRemove)
                                                     [v removeFromSuperview];

                                                 [self drawTouchPointsforDesignforDictionart:nowDict];
                                             });
                           });
        }
    }
}

-(void)drawTouchPointsforDesignforDictionart:(NSDictionary *)currectDesignDict
{
    NSArray *touchViews = [currectDesignDict valueForKey:@"touch_potistions"];
    for (NSDictionary *points in touchViews)
    {
        NSDictionary *position = [points valueForKey:@"position"];
        CGRect rect;
        rect.origin.x = [[position valueForKey:@"x"] intValue] ;
        rect.origin.y = [[position valueForKey:@"y"] intValue] ;
        rect.size.width = [[position valueForKey:@"width"] intValue] ;
        rect.size.height = [[position valueForKey:@"height"] intValue] ;
        
        UIButton *touchButton = [[UIButton alloc] initWithFrame:rect];
        touchButton.layer.borderWidth = 0.3;
        touchButton.layer.cornerRadius = 3;
        touchButton.layer.borderColor = [UIColor blackColor].CGColor;
        [touchButton setBackgroundColor:[UIColor greenColor]];
        [touchButton setAlpha:0.3];
        [touchButton setShowsTouchWhenHighlighted:YES];
        [touchButton addTarget:self
                        action:@selector(touchMethod:)
              forControlEvents:UIControlEventTouchDown];
        touchButton.tag = [[points valueForKey:@"destination_order"] intValue];
        [self.imageView addSubview:touchButton];
        [self.imageView bringSubviewToFront:touchButton];
    }
}

-(void)touchMethod:(UIButton *)sender
{
    int destinationImage = sender.tag;
    [self loadDesignAtIndex:destinationImage];
}


@end
