//
//  DesignShareGestureDelegate.h
//  DesignShare
//
//  Created by Bastin Raj on 11/11/13.
//  Copyright (c) 2013 Pramati. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DesignShareGestureDelegate <NSObject>

@required

- (void) componentTouchesBegan: (CGPoint) point;
- (void) componentTouchesMoved:(CGPoint) point;
- (void) componentTouchesEnded:(CGPoint) point;
- (void) componentTouchesCancelled:(CGPoint) point;

@end
