//
//  XMPPConnection.h
//  design-share-ios
//
//  Created by Bastin Raj on 12/2/13.
//  Copyright (c) 2013 Pramati. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XMPP.h"
#import "XMPPStream.h"
#import "PreviewControllerDelegate.h"


@interface XMPPConnection : NSObject <XMPPStreamDelegate>
{
    XMPPStream *xmppStream;
}

@property(nonatomic, readonly) XMPPStream *xmppStream;
@property(nonatomic, strong) NSString *password;
@property(nonatomic, retain) id<PreviewControllerDelegate> delegate;

- (BOOL) connect;
- (void) disconnect;
-(void) sendMessage: (NSString *) text toUser: (NSString *) username xPosition: (float) x  yPosition: (float) y  widthValue: (float) width heightValue: (float) height;

@end
