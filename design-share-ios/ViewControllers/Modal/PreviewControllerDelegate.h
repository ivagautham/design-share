//
//  PreviewControllerDelegate.h
//  design-share-ios
//
//  Created by Bastin Raj on 11/18/13.
//  Copyright (c) 2013 Pramati. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PreviewControllerDelegate <NSObject>


-(void) keyboardAppeared: (NSNotification *) notification;
-(void) keyboardDisappeared: (NSNotification *) notification;
-(void) userDidEnterFeedback: (NSString *) feedbackText;
-(void) feedbackCancelButtonClicked;
-(void) feedbackReceived: (NSString *) feedbackText xPosition: (float) x yPosition: (float) y  widthValue: (float) width  heightValue: (float) height;

@end
