//
//  XMPPConnection.m
//  design-share-ios
//
//  Created by Bastin Raj on 12/2/13.
//  Copyright (c) 2013 Pramati. All rights reserved.
//

#import "XMPPConnection.h"
#import "XMPP.h"
#import "XMPPStream.h"


#define XMPP_SERVER @"192.168.6.154"
//#define XMPP_SERVER @"Bastins-Mac-Mini.local"
//#define XMPP_SERVER @"localhost"

#define XMPP_SERVER_CONNECTION_PORT 5222


@implementation XMPPConnection

@synthesize xmppStream;
@synthesize password;
@synthesize delegate;


- (void)setupStream
{
    xmppStream = [[XMPPStream alloc] init];
    [xmppStream addDelegate:self delegateQueue:dispatch_get_main_queue()];
    
    xmppStream.hostName = XMPP_SERVER;
    xmppStream.hostPort = XMPP_SERVER_CONNECTION_PORT;
}


-(BOOL) connect
{
    [self setupStream];
    
    if ( ![xmppStream isDisconnected])
    {
        return YES;
    }
    
    NSString *userName = [NSString stringWithFormat:@"bastin@%@", XMPP_SERVER];
    password  = @"bastin";
    
    if (userName == nil || password == nil)
    {
        return NO;
    }
    
    XMPPJID *jid = [XMPPJID  jidWithString: userName];
    [xmppStream setMyJID: jid];
    
    NSLog(@"TEST: jid: %@", jid);
    
    NSError *error = nil;
    
    [xmppStream connectWithTimeout:XMPPStreamTimeoutNone error: &error];
    if (error)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:[NSString stringWithFormat:@"Can't connect to server %@", [error localizedDescription]]
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
        
        return NO;
    }

    return  YES;
    
}


- (void) disconnect
{
    [self goOffline];
    [xmppStream disconnect];
}


#pragma mark - XMPPStreamDelegate methods

- (void)xmppStreamDidConnect:(XMPPStream *)sender
{
    NSError *authError = nil;
    [xmppStream authenticateWithPassword:self.password error: &authError];
    
    if (authError)
    {
        NSLog(@"Error while authenticating with server: %@", authError);
    }
}

/**
 * This method is called after authentication has successfully finished.
 * If authentication fails for some reason, the xmppStream:didNotAuthenticate: method will be called instead.
 **/
- (void)xmppStreamDidAuthenticate:(XMPPStream *)sender
{
    NSLog(@"Authentication succeeds with server.");
    [self goOnline];
}

/**
 * This method is called if authentication fails.
 **/
- (void)xmppStream:(XMPPStream *)sender didNotAuthenticate:(NSXMLElement *)error
{
    NSLog(@"didNotAuthenticate. %@", error);
}

/* Sending data methods part
 */
- (void)xmppStream:(XMPPStream *)sender didSendIQ:(XMPPIQ *)iq
{
    
}

- (void)xmppStream:(XMPPStream *)sender didSendMessage:(XMPPMessage *)message
{
    NSLog(@"XMPP: didSendMessage: %@", message);
}

- (void)xmppStream:(XMPPStream *)sender didSendPresence:(XMPPPresence *)presence
{
    NSLog(@"XMPP: didSendPresence: %@", presence);
}


/*  Data receiving part
 */
/*
- (BOOL)xmppStream:(XMPPStream *)sender didReceiveIQ:(XMPPIQ *)iq
{
    
}
 */
- (void)xmppStream:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message
{
    NSLog(@"XMPP: didReceiveMessage: %@", message);
    NSLog(@"TEST: XMPP Message Type: %@", message.type);
    NSLog(@"TEST: XMPP Message Body: %@", message.body);
    NSLog(@"TEST: XMPP Message Name: %@", message.name);
    NSLog(@"TEST: XMPP Message XmlString: %@", message.XMLString);
    NSLog(@"TEST: XMPP Message From: %@", message.fromStr);

    [self readDataFromMessage: message];
}



- (void)xmppStream:(XMPPStream *)sender didReceivePresence:(XMPPPresence *)presence
{
    NSLog(@"XMPP: didReceivePresence: %@", presence);
}


- (void)goOnline
{
    XMPPPresence *presence = [XMPPPresence presence];
    [xmppStream sendElement: presence];
}


- (void)goOffline
{
    XMPPPresence *presence = [XMPPPresence presenceWithType:@"unavailable"];
    [[self xmppStream] sendElement:presence];
}


-(void) sendMessage: (NSString *) msgText toUser: (NSString *) username xPosition: (float) x  yPosition: (float) y  widthValue: (float) width heightValue: (float) height
{
    username = [NSString stringWithFormat:@"surya@%@", XMPP_SERVER];
    
    /*
    
    XMPPJID *toJid = [XMPPJID jidWithString: username];
    
    NSString *xmlString = [self convertMessageToXmlString: msgText xPosition:x yPosition:y widthValue:width heightValue:height];
    NSLog(@"TEST: xmlString: %@", xmlString);
    XMPPMessage *message = [XMPPMessage messageWithType:xmlString to: toJid];
    
    [xmppStream sendElement: message];
     */
    
    NSXMLElement *bodyElement = [NSXMLElement elementWithName:@"body" stringValue: msgText];
    [bodyElement addAttributeWithName:@"x" floatValue: x];
    [bodyElement addAttributeWithName:@"y" floatValue: y];
    [bodyElement addAttributeWithName:@"width" floatValue: width];
    [bodyElement addAttributeWithName:@"height" floatValue: height];
    
    
    NSXMLElement *msgElement = [NSXMLElement elementWithName:@"message"];
    [msgElement addAttributeWithName:@"type" stringValue:@"chat"];
    [msgElement addAttributeWithName:@"to" stringValue: username];
    [msgElement addChild:bodyElement];
    
    [xmppStream sendElement: msgElement];

}

/*
 Convert required data to xml string
 */

/*
-(NSString *) convertMessageToXmlString: (NSString *) msgText  xPosition: (float) x  yPosition: (float) y  widthValue: (float) width  heightValue: (float) height
{
    NSXMLElement *bodyElement = [NSXMLElement elementWithName:@"body" stringValue: msgText];
    [bodyElement addAttributeWithName:@"x" floatValue: x];
    [bodyElement addAttributeWithName:@"y" floatValue: y];
    [bodyElement addAttributeWithName:@"width" floatValue: width];
    [bodyElement addAttributeWithName:@"height" floatValue: height];
    
    
    NSXMLElement *msgElement = [NSXMLElement elementWithName:@"message"];
    [message addAttributeWithName:@"type" stringValue:@"chat"];
    [message addAttributeWithName:@"to" stringValue: ];
    [message addChild:body];
    
    
 
    

    return [msgElement XMLString];
}
*/


-(void) readDataFromMessage: (XMPPMessage *) message
{
    float x = 0;
    float y = 0;
    float width = 0.0;
    float height = 0.0;
    NSString *feedbackText = @"";


    NSXMLElement *feedbackElement = [message elementForName:@"body"];
    
    feedbackText = [feedbackElement stringValue];
    x = [feedbackElement attributeFloatValueForName:@"x"];
    y = [feedbackElement attributeFloatValueForName:@"y"];
    width = [feedbackElement attributeFloatValueForName:@"width"];
    height = [feedbackElement attributeFloatValueForName:@"height"];
    
    NSLog(@"After converting xml to value: %@ & %f & %f & %f & %f", feedbackText, x, y, width, height);

    // After receiving all draw feedback view if required, and show the text in thread
    [self.delegate feedbackReceived:feedbackText xPosition:x yPosition:y widthValue:width heightValue:height];
    
}

@end
