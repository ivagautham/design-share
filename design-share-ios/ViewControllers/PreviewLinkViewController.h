//
//  PreviewLinkViewController.h
//  design-share-ios
//
//  Created by Gautham on 10/12/13.
//  Copyright (c) 2013 Pramati. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PreviewLinkViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate>
{
    UIImage *toDisplayImage;
    NSMutableDictionary *displayImageDetails;
    IBOutlet UIImageView *displayImageView;
    NSMutableArray *allDesignImages;
    NSMutableArray *allDesignDetails;
    
    IBOutlet UIButton *backButton;
    BOOL previewOnly;
}

@property(nonatomic) IBOutlet UIScrollView *baseScrollView;

@property(nonatomic) UIImage *toDisplayImage;
@property(nonatomic) NSMutableDictionary *displayImageDetails;
@property(nonatomic) UIImageView *displayImageView;
@property(nonatomic) NSMutableArray *allDesignImages;
@property(nonatomic) NSMutableArray *allDesignDetails;

@property(nonatomic) UIButton *backButton;
@property(nonatomic) BOOL previewOnly;

@property(nonatomic) IBOutlet UICollectionView *allDesignsCollection;

-(IBAction)onBack:(id)sender;

@end
