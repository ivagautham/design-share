//
//  DocumentsDirectoryViewController.m
//  design-share-ios
//
//  Created by Gautham on 12/11/13.
//  Copyright (c) 2013 Pramati. All rights reserved.
//

#import "DocumentsDirectoryViewController.h"
#import "ImagePreViewCell.h"

@interface DocumentsDirectoryViewController ()

@end

@implementation DocumentsDirectoryViewController
@synthesize allDocumentImageNames,previewImages;
@synthesize noPicslbl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden: NO];
    [self loadAllImages];
}

-(void)loadAllImages
{
    if (!previewImages)
        previewImages = [[NSMutableArray alloc] init];
    [previewImages removeAllObjects];
    
    NSString *documentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    
    for (NSString *imageName in allDocumentImageNames)
    {
        NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDir,imageName];
        UIImage *newImage = [[UIImage alloc] initWithContentsOfFile:filePath];
        NSDictionary *attributes = [[NSFileManager defaultManager]
                                    attributesOfItemAtPath:filePath error:nil];
        NSString *size = [attributes valueForKey:NSFileSize];
        NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:newImage,@"Document_Image",imageName,@"file_path",@"documents_directory",@"source",size, NSFileSize, nil];
        [previewImages addObject:dict];
        

    }
    [self.collectionView reloadData];
}

-(IBAction)onCancel:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.6;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionReveal;
    transition.subtype = kCATransitionFromBottom;
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark -
#pragma mark UICollectionViewDataSource

-(NSInteger)numberOfSectionsInCollectionView:
(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView
    numberOfItemsInSection:(NSInteger)section
{
    if (previewImages.count > 0)
        noPicslbl.hidden = TRUE;
    else
        noPicslbl.hidden = FALSE;
    
    return previewImages.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ImagePreViewCell *myCell = [collectionView
                                dequeueReusableCellWithReuseIdentifier:@"IPVCell"
                                forIndexPath:indexPath];
    
    UIImage *image;
    NSInteger row = [indexPath row];
    NSDictionary *dict = [self.previewImages objectAtIndex:row];
    image = [dict objectForKey:@"Document_Image"];
    
    myCell.imageView.image = image;
    [myCell setFrame:CGRectMake(myCell.frame.origin.x, myCell.frame.origin.y, image.size.width, image.size.height)];
    
    return myCell;
}

#pragma mark -
#pragma mark UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = [indexPath row];
    NSDictionary *dict = [self.previewImages objectAtIndex:row];
    [_delegate didSelectItem:dict];
    
    [self.navigationController popViewControllerAnimated:YES];

}

@end
