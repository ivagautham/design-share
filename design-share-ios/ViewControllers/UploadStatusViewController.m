//
//  UploadStatusViewController.m
//  design-share-ios
//
//  Created by Gautham on 13/11/13.
//  Copyright (c) 2013 Pramati. All rights reserved.
//

#import "UploadStatusViewController.h"

@interface UploadStatusViewController ()

@end

@implementation UploadStatusViewController
@synthesize statusImage,statusString,designerPin,reviewerPin,shareButton;
@synthesize designerlbl,reviewerlbl;
@synthesize status,responseObject;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)onDone:(id)sender
{
    [self dismissViewControllerAnimated:TRUE completion:nil];
    [self.delegate didFinishUploading];
}

-(IBAction)onShare:(id)sender
{
    NSArray *activityItems = [NSArray arrayWithObjects:[NSString stringWithFormat:@"Here is the pin for the design I uploaded to the design-share-app \"%@\", please leave you review and feedback",self.reviewerPin.text], nil];
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    [self presentViewController:activityController animated:YES completion:nil];
}

-(void)setUpControls
{
    if (!statusImage)
        statusImage = [[UIImageView alloc] init];
    if (!statusString)
        statusString = [[UILabel alloc] init];
    if (!designerPin)
        designerPin = [[UILabel alloc] init];
    if (!reviewerPin)
        reviewerPin = [[UILabel alloc] init];
    if (!designerlbl)
        designerlbl = [[UILabel alloc] init];
    if (!reviewerlbl)
        reviewerlbl = [[UILabel alloc] init];
    if (!shareButton)
        shareButton = [[UIBarButtonItem alloc] init];
}

-(void) updateStatus:(NSString *)succ withResponse:(NSDictionary *)dict
{
    if ([succ isEqualToString:NOTIFICATION_UPLOAD_DESIGN_FAILED] )
    {
        self.statusImage.image = [UIImage imageNamed:@"ico_failed"];
        self.statusString.text = @"Failed to upload the design. Please try again";
        self.designerPin.hidden = TRUE;
        self.reviewerPin.hidden = TRUE;
        self.designerlbl.hidden = TRUE;
        self.reviewerlbl.hidden = TRUE;
        self.shareButton.enabled = FALSE;
    }
    else if ([succ isEqualToString:NOTIFICATION_UPLOAD_DESIGN_SUCCESS] )
    {
        self.statusImage.image = [UIImage imageNamed:@"ico_success"];
        self.statusString.text = @"The design was uploaded successfully.";
        self.designerPin.hidden = FALSE;
        self.reviewerPin.hidden = FALSE;
        self.designerlbl.hidden = FALSE;
        self.reviewerlbl.hidden = FALSE;
        self.shareButton.enabled = TRUE;
        
        self.designerPin.text = [responseObject valueForKey:@"designer_pin"];
        self.reviewerPin.text = [responseObject valueForKey:@"reviewer_pin"];
    }

    else if ([succ isEqualToString:NOTIFICATION_DOWNLOAD_DATA_FAILED] )
    {
        self.statusImage.image = [UIImage imageNamed:@"ico_failed"];
        self.statusString.text = @"Invalid pin. Please try again";
        self.designerPin.hidden = TRUE;
        self.reviewerPin.hidden = TRUE;
        self.designerlbl.hidden = TRUE;
        self.reviewerlbl.hidden = TRUE;
        self.shareButton.enabled = FALSE;
    }
    else if ([succ isEqualToString:NOTIFICATION_FAIL_DOWNLOAD_IMAGE] )
    {
        self.statusImage.image = [UIImage imageNamed:@"ico_failed"];
        self.statusString.text = @"Unable to download design from the given URL. Please try again";
        self.designerPin.hidden = TRUE;
        self.reviewerPin.hidden = TRUE;
        self.designerlbl.hidden = TRUE;
        self.reviewerlbl.hidden = TRUE;
        self.shareButton.enabled = FALSE;
    }

}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden: NO];
    [self updateStatus:status withResponse:nil];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
