//
//  ViewController.h
//  DesignShare
//
//  Created by Bastin Raj on 11/8/13.
//  Copyright (c) 2013 Pramati. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DesignGestureRecognizer.h"
#import "DesignShareGestureDelegate.h"
#import "WEPopoverController.h"
#import "PreviewControllerDelegate.h"
#import "FeedbackViewController.h"
#import "XMPPConnection.h"

@interface PreviewViewController : UIViewController<PreviewControllerDelegate, WEPopoverControllerDelegate, UIActionSheetDelegate>
{
    id currentDrawnView;
    FeedbackViewController *feedbackController;
    UIView *currentSelectedView;
    NSMutableDictionary *feedbacks;
    XMPPConnection *connection;
    IBOutlet UIView *titlebar;
    NSMutableArray *designArray;
    
    NSString *designAuthor;
    NSString *authorEmail;
}

@property(nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property(nonatomic, retain) UIImageView *imageView;
@property(nonatomic, assign) CGPoint startAreaPoint;
@property(nonatomic, strong) WEPopoverController  *popoverController;
@property(nonatomic, strong) UIView *titlebar;

@property(nonatomic, strong) NSMutableArray *designArray;

@property(nonatomic, strong) NSString *designAuthor;
@property(nonatomic, strong) NSString *authorEmail;

-(IBAction) logoutAction:(id)sender;


-(void)setupImage:(NSMutableDictionary *)dict;

@end
