//
//  DocumentsDirectoryViewController.h
//  design-share-ios
//
//  Created by Gautham on 12/11/13.
//  Copyright (c) 2013 Pramati. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DocumentsDirectoryDelegate;

@interface DocumentsDirectoryViewController : UICollectionViewController<UICollectionViewDataSource,UICollectionViewDelegate>
{
    NSMutableArray *allDocumentImageNames;
    IBOutlet UILabel *noPicslbl;

}

@property (nonatomic, weak) id<DocumentsDirectoryDelegate> delegate;

@property (strong, nonatomic) NSMutableArray *allDocumentImageNames;
@property (strong, nonatomic) NSMutableArray *previewImages;
@property (strong, nonatomic) UILabel *noPicslbl;

-(IBAction)onCancel:(id)sender;

@end

@protocol DocumentsDirectoryDelegate <NSObject>
@optional

- (void)didSelectItem:(NSDictionary *)itemDictionary;

@end
