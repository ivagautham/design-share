//
//  FeedbackViewController.m
//  DesignShare
//
//  Created by Bastin Raj on 11/18/13.
//  Copyright (c) 2013 Pramati. All rights reserved.
//

#import "FeedbackViewController.h"


@implementation FeedbackViewController

@synthesize delegate;
@synthesize feedbackTextView;

- (id)initWithStyle:(UITableViewStyle) style withFeedbackText: (NSString *) enteredText
{
    // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
    if ((self = [super initWithStyle:style]))
    {
		self.preferredContentSize = CGSizeMake(200, 115);
        alreadyEnteredFeedbackText = enteredText;
    }
    return self;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self registerKeyboardNotification];
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: YES];
    [self.navigationController setNavigationBarHidden: NO];
}

-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear: YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) registerKeyboardNotification
{
    [[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(keyboardDidAppear:) name:UIKeyboardDidShowNotification object: nil];
    [[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(keyboardDidDisappear:) name:UIKeyboardDidHideNotification object: nil];
}

-(void) unRegisterKeyboardNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object: nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object: nil];
}

-(void) keyboardDidAppear: (NSNotification *) notification
{
    [self.delegate keyboardAppeared: notification];
}

-(void) keyboardDidDisappear: (NSNotification *) notification
{
    [self.delegate keyboardDisappeared: notification];
}

-(IBAction) submitButtonAction: (id)sender
{
    [self.delegate userDidEnterFeedback: feedbackTextView.text];
}

#pragma mark - UITableViewDataSourceDelegate methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    if (indexPath.row == 0)
    {
        feedbackTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 200, 90)];
        [feedbackTextView setBackgroundColor:[UIColor lightGrayColor]];
        
        if (alreadyEnteredFeedbackText != nil && ![alreadyEnteredFeedbackText isEqualToString:@""])
        {
            self.feedbackTextView.text = alreadyEnteredFeedbackText;
        }

        
        [cell addSubview: feedbackTextView];
    }
    else if (indexPath.row == 1)
    {
        UIButton *submitButton = [[UIButton alloc] initWithFrame:CGRectMake(5, 0, 100, 25)];
        [submitButton setBackgroundColor:[UIColor clearColor]];
        
        [submitButton setTitle:@"Submit" forState:UIControlStateNormal];
        [submitButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        [submitButton addTarget:self action:@selector(submitButtonAction:) forControlEvents: UIControlEventTouchUpInside];
        [cell addSubview: submitButton];
        
        // Cancel button
        UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(110, 0, 100, 25)];
        [cancelButton setBackgroundColor:[UIColor clearColor]];
        
        [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
        [cancelButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        [cancelButton addTarget:self action:@selector(cancelButtonAction:) forControlEvents: UIControlEventTouchUpInside];
        [cell addSubview: cancelButton];
    }
    
    return cell;
}


#pragma mark - UITableViewDelegate methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        return 90;
    }
    else
    {
        return 25;
    }
}

-(void) cancelButtonAction: (id)sender
{
    [self.delegate feedbackCancelButtonClicked];
}

@end
