//
//  UploadDetailsViewController.h
//  design-share-ios
//
//  Created by Gautham on 11/11/13.
//  Copyright (c) 2013 Pramati. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UploadStatusViewController.h"
#import "DocumentsDirectoryViewController.h"

@interface UploadDetailsViewController : UIViewController<UITextFieldDelegate, UploadStatusDelegate, UICollectionViewDataSource, UICollectionViewDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate, DocumentsDirectoryDelegate,UIAlertViewDelegate>
{
    UIImage *imageToUpdate;
    NSDictionary *imageData;
    UIActionSheet *uploadOptionSheet;
    UIImagePickerController * picker;
}

@property(nonatomic) UIActionSheet *uploadOptionSheet;

@property(nonatomic) IBOutlet UIButton *imageView;
@property(nonatomic) IBOutlet UITextField *imageName;
@property(nonatomic) IBOutlet UILabel *imageResolution;
@property(nonatomic) IBOutlet UILabel *imageAuthor;
@property(nonatomic) IBOutlet UILabel *imageAuthorMail;
@property(nonatomic) IBOutlet UILabel *imageType;
@property(nonatomic) IBOutlet UILabel *imageFor;
@property(nonatomic) IBOutlet UILabel *imageSize;
@property(nonatomic) IBOutlet UILabel *orientation;
@property(nonatomic) IBOutlet UILabel *source;
@property(nonatomic) IBOutlet UILabel *sourcePath;

@property(nonatomic) IBOutlet UITextField *author;
@property(nonatomic) IBOutlet UITextField *email;

@property(nonatomic) IBOutlet UIBarButtonItem *postDesign;
@property(nonatomic) IBOutlet UIButton *trash;
@property(nonatomic) IBOutlet UIButton *link;

@property(nonatomic) IBOutlet UICollectionView *allDesignsCollection;
@property(nonatomic) NSMutableArray *allDesignsImageArray;
@property(nonatomic) NSMutableArray *allDesignsDetailArray;

@property(nonatomic) UIImage *imageToUpdate;
@property(nonatomic) NSDictionary *imageData;

-(IBAction)onPostDesign:(id)sender;
-(IBAction)onCancel:(id)sender;
-(IBAction)ontrash:(id)sender;
-(IBAction)onlink:(id)sender;
-(IBAction)onFullScreen:(id)sender;


@end
