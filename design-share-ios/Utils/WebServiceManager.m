//
//  WebServiceManager.m
//  aires
//
//  Created by Gautham on 18/04/13.
//  Copyright (c) 2013 Imaginea. All rights reserved.
//

#import "WebServiceManager.h"
#import "AFNetworking.h"

#define mSingleton 	((AiresSingleton *) [AiresSingleton getSingletonInstance])

#define SERVICE_URL @"http://192.168.6.41"

@implementation WebServiceManager

- (id)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    return self;
}

-(void)downloadImageforURL:(NSString *)url
{
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    
    AFImageRequestOperation *operation;
    
    operation = [AFImageRequestOperation imageRequestOperationWithRequest:request
                                                     imageProcessingBlock:nil
                                                                  success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                                                      NSLog(@"Success req:%@ andResponse:%@",request,response);
                                                                      NSString *filename = [[response.URL path] lastPathComponent];
                                                                      NSString *size =[response.allHeaderFields valueForKey:@"Content-Length"];
                                                                      NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:image,@"Document_Image",filename,@"file_path",@"url",@"source",size, NSFileSize,url,@"image_url", nil];
                                                                      [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_DID_DOWNLOAD_IMAGE object:dict];
                                                                  }
                                                                  failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                                                      [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_FAIL_DOWNLOAD_IMAGE object:nil];
                                                                      
                                                                      NSLog(@"%@", [error localizedDescription]);
                                                                  }];
    [operation start];
}

-(void)loginwithPin:(NSString *)pin
{
    NSDictionary *projectDictionary;
    if (pin.length <5)
        projectDictionary = [NSDictionary dictionaryWithObjectsAndKeys:pin,@"pin",@"reviewer",@"pin_type", nil];
    else
        projectDictionary = [NSDictionary dictionaryWithObjectsAndKeys:pin,@"pin",@"author",@"pin_type", nil];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:projectDictionary
                                                       options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];

    NSMutableDictionary *parameterDictionary = [NSMutableDictionary dictionaryWithObject:jsonString forKey:@"input"];
    
    NSURL *url = [NSURL URLWithString:SERVICE_URL];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    [httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
    
    //[httpClient setDefaultHeader:@"Accept" value:@"application/json"];
   // NSMutableURLRequest *request1 = [httpClient requestWithMethod:@"POST" path:@"/design-share-new/login" parameters:parameterDictionary];
    
    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST" path:@"/design-share-api/loginformutipledesigns" parameters:parameterDictionary constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
    }];

    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"Response: %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
         
         NSError* error;
         NSDictionary* response = [NSJSONSerialization
                                   JSONObjectWithData:responseObject
                                   options:kNilOptions
                                   error:&error];
         
         NSString *status = [response valueForKey:@"status"];
         if (!status || [status isEqualToString:@"failed"])
             [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_DOWNLOAD_DATA_FAILED object:nil];
         else
             [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_DOWNLOAD_DATA_SUCCESS object:response];
         
     }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
         [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_DOWNLOAD_DATA_FAILED object:nil];
         
     }];
    
    ;
    [operation start];
}

-(void)uploadProject:(NSDictionary *)projectDictionary andDesign:(NSData *)imageData
{
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:projectDictionary
                                                       options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];

    NSDictionary *parameterDictionary = [NSDictionary dictionaryWithObject:jsonString forKey:@"input"];
    
    NSString *fileName = [projectDictionary valueForKey:@"name"];
    
    NSString *mimeType;
    NSArray *items = [fileName componentsSeparatedByString:@"."];
    if ([items count] > 0)
        mimeType = [NSString stringWithFormat:@"image/%@",[items objectAtIndex:1]];
    else
        mimeType = [NSString stringWithFormat:@"image/jpeg"];
    
    NSURL *url = [NSURL URLWithString:SERVICE_URL];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    
    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST" path:@"/design-share-api/design" parameters:parameterDictionary constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
        [formData appendPartWithFileData:imageData name:@"uploadedfile" fileName:fileName mimeType:mimeType];
    }];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Response: %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
        
        NSError* error;
        NSDictionary* response = [NSJSONSerialization
                                  JSONObjectWithData:responseObject
                                  options:kNilOptions
                                  error:&error];
        NSString *status = [response valueForKey:@"status"];
        if (!status || [status isEqualToString:@"failed"])
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_UPLOAD_DESIGN_FAILED object:error];
        }
        else
        {
            NSString *response = [operation responseString];
            NSLog(@"response: [%@]",response);
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_UPLOAD_DESIGN_SUCCESS object:response];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if([operation.response statusCode] == 403){
            NSLog(@"Upload Failed");
            return;
        }
        NSLog(@"error: %@", [operation error]);
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_UPLOAD_DESIGN_FAILED object:error];
        
    }];
    
    [operation start];
    
}
@end
