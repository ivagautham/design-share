//
//  WebServiceManager.h
//  aires
//
//  Created by Gautham on 18/04/13.
//  Copyright (c) 2013 Imaginea. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WebServiceManager : NSObject
{
    NSMutableDictionary *keyedAiresServices;
    NSString *servicesPlistPath;
}
-(void)downloadImageforURL:(NSString *)url;

-(void)loginwithPin:(NSString *)pin;
-(void)uploadProject:(NSDictionary *)projectDictionary andDesign:(NSData *)imageData;

@end
