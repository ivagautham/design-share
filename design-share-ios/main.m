//
//  main.m
//  design-share-ios
//
//  Created by Gautham on 11/11/13.
//  Copyright (c) 2013 Pramati. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
